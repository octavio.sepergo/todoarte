package beans;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;



import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import clases.Artista;
import clases.Fan;
import clases.Usuario;

@Named(value = "RegistroFanBean")
@RequestScoped
@TransactionManagement(TransactionManagementType.BEAN)
public class RegistroFanBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String nick;

	private String nickErr;


	private String pass;
	private String correo;
	private String sexoEleg;
	
	private List<SelectItem> listaS;
	
	
	private List<SelectItem> catItem;
	
	
	@PostConstruct
	public void init() {

		listaS = new ArrayList<SelectItem>();
		listaS.add(new SelectItem(1, "Hombre"));
		listaS.add(new SelectItem(2, "Mujer"));
		
		Client client = ClientBuilder.newClient();
		WebTarget target = client.target ("http://localhost:8080/TodoArte/api/Usuario").path("/listarCategorias");  
		Response res = target.request().get();
		Type type = new TypeToken<List<String>>(){}.getType();
		String listString= res.readEntity(String.class);
		Gson gson=new Gson();	
		List<String> cat = gson.fromJson(listString,type);
		
		catItem = new ArrayList<SelectItem>();
		int cont = 0;
		for(String str : cat)
		{
			cont++;
			catItem.add(new SelectItem(cont, str)); 
		}
	}
	public RegistroFanBean() {

	}

	public String getNickErr() {
		return nickErr;
	}
	public void setNickErr(String nickErr) {
		this.nickErr = nickErr;
	}

	public String getNick() {
		return nick;
	}
	public void setNick(String nick) {
		this.nick = nick;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	public String getSexoEleg() {
		return sexoEleg;
	}
	public void setSexoEleg(String sexoEleg) {
		this.sexoEleg = sexoEleg;
	}
	public List<SelectItem> getListaS() {
		return listaS;
	}
	public void setListaS(List<SelectItem> listaS) {
		this.listaS = listaS;
	}
	public List<SelectItem> getCatItem() {
		return catItem;
	}
	public void setCatItem(List<SelectItem> catItem) {
		this.catItem = catItem;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public void registroFan() {
	


	}

}
