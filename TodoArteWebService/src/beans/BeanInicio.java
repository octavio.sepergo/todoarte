package beans;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.javafx.binding.SelectBinding.AsDouble;

import clases.Artista;
import clases.Usuario;


@Named("BeanInicio")
@RequestScoped
public class BeanInicio implements Serializable{
	
	
	private static final long serialVersionUID = 413L;
	private Date fecha;
	private String nick;
	List<String> notis;
	

	
	public int notificacionesNuevas(String Nick) {
			
		Client client = ClientBuilder.newClient();
		WebTarget target = client.target ("http://localhost:8080/TodoArte/api/notis").path("/notificacionesNuevas");  
		Response res = target.request().post(Entity.json(Nick));
		Type type = new TypeToken<Usuario>(){}.getType();
		String listString= res.readEntity(String.class);
		Gson gson=new Gson();	
		Usuario us2 = gson.fromJson(listString,type);
		return us2.getNotificacionesNuevas();

	}
	
	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}

	public BeanInicio() {
		
	}
	
	
	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String nickSecion() {
		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		HttpSession session = request.getSession();
		if (session != null && session.getAttribute("user") != null) {
			Usuario us = (Usuario) session.getAttribute("user");
			return us.getNick();
		}; 
		return "NO LOGEADO";
	};
	
	public List<String> notis() {
		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		HttpSession session = request.getSession();
		if (session != null && session.getAttribute("user") != null) {
			Usuario us = (Usuario) session.getAttribute("user");
			us.setNotificacionesNuevas(0);
			return us.getNotificaciones();
		}; 
		return null;
	}

	public List<String> getNotis() {
		return notis;
	}

	public void setNotis(List<String> notis) {
		this.notis = notis;
	};
	
	public void programarQyA() {

		FacesMessage message = null;
        boolean loggedIn = true;
		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		HttpSession session = request.getSession();
		Usuario us = (Usuario) session.getAttribute("user");	
        message = new FacesMessage(FacesMessage.SEVERITY_INFO, "QyA","programada");
        FacesContext.getCurrentInstance().addMessage(null, message);

	};
	
	
}
