package beans;


import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import clases.Artista;
import clases.DtLogin;
import clases.Fan;
import clases.Usuario;


@Named(value = "LoginBean")
@RequestScoped
public class LoginBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String nick;
	private String pass;




	public LoginBean() {
		// TODO Auto-generated constructor stub
	}

	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}


    public String login() {
    	FacesMessage message = null;
        boolean loggedIn = false;
        DtLogin data = new DtLogin(pass,nick);
        
    	Client client = ClientBuilder.newClient();
    	WebTarget target = client.target ("http://localhost:8080/TodoArte/api/Seguridad").path("/existeUsuario");  
    	Response res = target.request().post(Entity.json(data));
    	Type type = new TypeToken<Usuario>(){}.getType();
    	String listString= res.readEntity(String.class);
    	Gson gson=new Gson();	
    	System.out.println(listString);
    	Usuario user = gson.fromJson(listString,type);

        FacesContext context = FacesContext.getCurrentInstance();  
        if (user == null) {
            context.addMessage(null, new FacesMessage("Datos incorrectos, intentelo de nuevo"));
            nick = null;
            pass = null;
            return null;
        } else {
        	if(user instanceof Artista) {
                context.getExternalContext().getSessionMap().put("user", user); 
                loggedIn = true;
                message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Bienvenido", nick);
                FacesContext.getCurrentInstance().addMessage(null, message);

        	}else if(user instanceof Fan) {
        	
                context.getExternalContext().getSessionMap().put("user", user);  
                loggedIn = true;
                message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Bienvenido", nick);
                FacesContext.getCurrentInstance().addMessage(null, message);

              
        	}
        	return "Error extra�o";
        }
    }

    public String logout() {
    	FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
    	return "/principalindex.xhtml?faces-redirect=true";
    }

}