package beans;

import java.io.Serializable;

import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import clases.Artista;
import clases.Fan;
import clases.Usuario;

@Named(value = "BeanDeSeguridad")
@RequestScoped
public class BeanDeSeguridad implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 133L;

	
	
	public BeanDeSeguridad() {

	}
	

	public Boolean isSecion() {
		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		HttpSession session = request.getSession();
		return (session != null && session.getAttribute("user") != null);
		
	};
	
	public String getNick() {
		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		HttpSession session = request.getSession();
		if (session != null && session.getAttribute("user") != null) {
			Usuario us = (Usuario) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(session.getAttribute("user"));
			return us.getNick();
		}; 
		return "NO LOGEADO";
	};
	
	public Boolean isSecionFan() 
	{	System.out.println("entrada fan");
		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		HttpSession session = request.getSession();
		System.out.println(session.getAttribute("user"));
		if (session != null && session.getAttribute("user") != null) {
			Usuario us = (Usuario) session.getAttribute("user");
			System.out.println(us.getNick());
			if (us instanceof Fan) {

				  return true;
				}
			else {
				  return false;
				}

		}
		return false; 

	};
	
	public Boolean isSecionArtista() {
		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		HttpSession session = request.getSession();
		if (session != null && session.getAttribute("user") != null) {
			Usuario us = (Usuario) session.getAttribute("user");
			if (us instanceof Artista) {
				  return true;
				}
			else {
				  return false;
				}


		}
		 return false;

	};
	
	

}
