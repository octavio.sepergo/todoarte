package beans;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.ArrayList;

import java.util.List;

import javax.annotation.PostConstruct;

import javax.ejb.EJB;

import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.enterprise.context.RequestScoped;


import javax.faces.model.SelectItem;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import clases.Artista;
import clases.Categoria;
import clases.Fan;
import clases.Sitio;



@Named(value = "ArtistaBean")
@RequestScoped

@TransactionManagement(TransactionManagementType.BEAN)
public class ArtistaBean implements Serializable{

	private List<Artista> artistas;
	private List<Artista> artistasCat;
	private List<SelectItem> catItem;
	List<Categoria> cate;
	List<Categoria> cate2;
	List<Categoria> cate3;
	private String catFav;
	private String nick= "";
	private String nick2 = "nick..";
	private String correo = "correo..";
	private String direccion = "direccion..";
	private String nombre = "nombre..";
	private String pass = "pass..";
	private String msj = "Resultado de Busqueda...";
	private String msj2 = "Bienvenido..";
	private String msj3 = "Resultado de Busqueda...";
	private int id = 6;
	

	private static final long serialVersionUID = 1L;

	public ArtistaBean() {
		
	}
	
	public List<Categoria> getCate() {
		return cate;
	}
	public void setCate(List<Categoria> cate) {
		this.cate = cate;
	}
	public List<Categoria> getCate2() {
		return cate2;
	}
	public void setCate2(List<Categoria> cate2) {
		this.cate2 = cate2;
	}
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@PostConstruct
	public void init() {
		
		System.out.println("aaaaaaaaaaa");
		Client client = ClientBuilder.newClient();
		WebTarget target = client.target ("http://localhost:8080/TodoArte/api/Usuario/listarCategorias");  
		Response res = target.request().get();
		Type type = new TypeToken<List<String>>(){}.getType();
		String listString= res.readEntity(String.class);
		System.out.println(listString);
		Gson gson=new Gson();	
		List<String> cat = gson.fromJson(listString,type);
		System.out.println("bbbbbbbbbbbbbbbbbbbb");
		

        catItem = new ArrayList<SelectItem>();
        int cont = 0;
        for(String str : cat)
        {
            cont++;
            catItem.add(new SelectItem(cont, str)); 
        }
        System.out.println("ccccccccccccccccccccc");
		WebTarget target2 = client.target ("http://localhost:8080/TodoArte/api/Usuario").path("/listarArtistasUnban");  
		Response res2 = target2.request().get();
		Type type2 = new TypeToken<List<Artista>>(){}.getType();
		String listString2= res2.readEntity(String.class);
		System.out.println(listString2);
		Gson gson2=  new GsonBuilder().setDateFormat("yyyy-MM-ddZ").create();
		this.artistas = gson2.fromJson(listString2,type2);

		this.catFav = "1";
		int foo = Integer.parseInt(this.catFav);
		System.out.println("dddddddddddddddddddddddddddd");
		WebTarget target3 = client.target ("http://localhost:8080/TodoArte/api/Usuario").path("/BuscarArtistaCategoria");  
		Response res3 = target3.request().post(Entity.json(foo));
		Type type3 = new TypeToken<List<Artista>>(){}.getType();
		String listString3= res3.readEntity(String.class);
		System.out.println(listString3);
		Gson gson3 =new Gson();	
		this.artistasCat = gson3.fromJson(listString3,type3);
		
		System.out.println("fffffffffffffffffffffffffffffffff");
		
	}
		
	
	public List<Artista> getArtistasCat() {
		return artistasCat;
	}
	public void setArtistasCat(List<Artista> artistasCat) {
		this.artistasCat = artistasCat;
	}
	public List<SelectItem> getCatItem() {
		return catItem;
	}
	public void setCatItem(List<SelectItem> catItem) {
		this.catItem = catItem;
	}
	public String getCatFav() {
		return catFav;
	}
	public void setCatFav(String catFav) {
		this.catFav = catFav;
	}
	public List<Artista> getArtistas() {
		return artistas;
	}

	public void setArtistas(List<Artista> artistas) {
		this.artistas = artistas;
	}

	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}	

	public List<Categoria> getCate3() {
		return cate3;
	}

	public void setCate3(List<Categoria> cate3) {
		this.cate3 = cate3;
	}

	public String getMsj() {
		return msj;
	}

	public void setMsj(String msj) {
		this.msj = msj;
	}
	
	
	public String getNick2() {
		return nick2;
	}

	public void setNick2(String nick2) {
		this.nick2 = nick2;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public String getMsj2() {
		return msj2;
	}

	public void setMsj2(String msj2) {
		this.msj2 = msj2;
	}
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMsj3() {
		return msj3;
	}
	public void setMsj3(String msj3) {
		this.msj3 = msj3;
	}
	

	public void list() {
		
		Client client = ClientBuilder.newClient();
		WebTarget target = client.target ("http://localhost:8080/TodoArte/api/Usuario").path("/listarArtistasUnban");  
		Response res = target.request().get();
		Type type = new TypeToken<List<Artista>>(){}.getType();
		String listString = res.readEntity(String.class);
		Gson gson=new Gson();	
		this.artistas = gson.fromJson(listString,type);
		

	}
	
	public void buscar() {
		
		
		Client client = ClientBuilder.newClient();
		WebTarget target3 = client.target ("http://localhost:8080/TodoArte/api/Usuario").path("/buscarArtista");  
		Response res3 = target3.request().post(Entity.json(this.nick));
		Type type3 = new TypeToken<Artista>(){}.getType();
		String listString3= res3.readEntity(String.class);
		Gson gson3 =new Gson();	
		Artista n = gson3.fromJson(listString3,type3);
			
        if(n!=null) {
        	List<Artista> cat2 = new ArrayList<Artista>();
        	cat2.add(n);
	    	this.artistasCat = cat2;
	    	
	        System.out.println("Encontrado");
        }else {
	        System.out.println("Artista No encontrado");
	        
	        this.setMsj("No se han encontrado Resultado que coincida");
	        

			WebTarget target = client.target ("http://localhost:8080/TodoArte/api/Usuario").path("/BuscarArtistaRelacionBusqueda");  
			Response res = target.request().post(Entity.json(this.nick));
			Type type = new TypeToken<List<Artista>>(){}.getType();
			String listString= res.readEntity(String.class);
			Gson gson =new Gson();	
			this.artistasCat = gson.fromJson(listString,type);
	        
        }
	  
	}
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void buscarArtCat() throws Exception{
		 System.out.println(this.catFav);
		int foo = Integer.parseInt(this.catFav);
		Client client = ClientBuilder.newClient();
		WebTarget target3 = client.target ("http://localhost:8080/TodoArte/api/Usuario").path("/BuscarArtistaCategoria");  
		Response res3 = target3.request().post(Entity.json(foo));
		Type type3 = new TypeToken<List<Artista>>(){}.getType();
		String listString3= res3.readEntity(String.class);
		Gson gson3 =new Gson();	
		this.artistasCat = gson3.fromJson(listString3,type3);
		


	}

}
