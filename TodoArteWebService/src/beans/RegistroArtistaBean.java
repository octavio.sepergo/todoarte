package beans;

import java.io.Serializable;
import java.lang.reflect.Type;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;



import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import clases.Artista;
import clases.Categoria;
import clases.Fan;
import clases.Sitio;
import clases.Usuario;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;
@Named(value = "RegistroArtistaBean")
@SessionScoped

@TransactionManagement(TransactionManagementType.BEAN)
public class RegistroArtistaBean implements Serializable{
	


	private static final long serialVersionUID = 1L;

	private String nick;
	private String pass;
	private String correo;
	private String direccion;
	private String nombre;
	private String sexoEleg;
	private Date fecha;
	private String titulo;
	private String des;
	
	private String catFav;
	private List<String> catFavItems;
	private List<SelectItem> listaS;
	private List<SelectItem> catItem;
	
	private String msj= "Enorabuena por registrarte";
	@PostConstruct
	public void init() {
		//lista de colores utilizando la classe SelecItem
		listaS = new ArrayList<SelectItem>();
		listaS.add(new SelectItem(1, "Hombre"));
		listaS.add(new SelectItem(2, "Mujer"));
		
		Client client = ClientBuilder.newClient();
		WebTarget target = client.target ("http://localhost:8080/TodoArte/api/Usuario").path("/listarCategorias");  
		Response res = target.request().get();
		Type type = new TypeToken<List<String>>(){}.getType();
		String listString= res.readEntity(String.class);
		Gson gson=new Gson();	
		List<String> cat = gson.fromJson(listString,type);
		
		catItem = new ArrayList<SelectItem>();
		int cont = 0;
		for(String str : cat)
		{
			cont++;
			catItem.add(new SelectItem(cont, str)); 
		}
	}
	
 
	public RegistroArtistaBean() {
		// TODO Auto-generated constructor stub
	}
	

	public Date getFecha() {
		return fecha;
	}


	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}



	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getSexoEleg() {
		return sexoEleg;
	}

	public void setSexoEleg(String sexoEleg) {
		this.sexoEleg = sexoEleg;
	}

	public List<SelectItem> getListaS() {
		return listaS;
	}

	public void setListaS(List<SelectItem> listaS) {
		this.listaS = listaS;
	}
	
	
	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getDes() {
		return des;
	}

	public void setDes(String des) {
		this.des = des;
	}

	
	public String getCatFav() {
		return catFav;
	}

	public void setCatFav(String catFav) {
		this.catFav = catFav;
	}

	public List<SelectItem> getCatItem() {
		return catItem;
	}

	public void setCatItem(List<SelectItem> catItem) {
		this.catItem = catItem;
	}
	
	

	public String getMsj() {
		return msj;
	}

	public void setMsj(String msj) {
		this.msj = msj;
	}

	public List<String> getCatFavItems() {
		return catFavItems;
	}

	public void setCatFavItems(List<String> catFavItems) {
		this.catFavItems = catFavItems;
	}

	public void registrar() {
		

	}

}
