package clases;

public class DtLogin {

	public String pass;
	public String nick;
	
	public DtLogin() {

	}
	
	public DtLogin(String pass, String nick) {
		super();
		this.pass = pass;
		this.nick = nick;
	}

	public String getPass() {
		return pass;
	}

	public String getNick() {
		return nick;
	}

	
	
}
