package clases;

import java.sql.Date;

public class DtComentario {

	private String nick;

	private String texto;

	private String fecha;
	
	public DtComentario(String nick,String texto,String fecha){
		this.nick = nick;
		this.texto = texto;
		this.fecha = fecha;
		
	}

	public String getNick() {
		return nick;
	}

	public String getTexto() {
		return texto;
	}

	public String getFecha() {
		return fecha;
	}
	
}
