package clases;

import java.io.Serializable;
import java.lang.String;
import javax.persistence.*;

import java.util.ArrayList;
import java.util.List;





public class Sitio implements Serializable {

	   
	private int sitioId;
	
	private String titulo;
	
	private String descripcion;
	
	private List<Paquete> paquetes;

	private List<Categoria> categorias;
	
	private List<Fan> fans;
	
	private List<FanPremium> fansPremium;
	
	private List<Seccion> secciones;
	
	private List<String> listaNegra;
	

	private boolean QA_activo;
	
	private static final long serialVersionUID = 1L;

	public Sitio() {
		super();
	}
	
	public Sitio(String des,String tit) {
		super();	
		this.descripcion = des;
		this.titulo = tit;
		this.QA_activo = false;
		Categoria c = Persistence.createEntityManagerFactory("TodoArte").createEntityManager().find(Categoria.class, 1);
		List<Categoria> l = new  ArrayList<Categoria>();
		l.add(c);
		this.setCategorias(l);
		Paquete p = new Paquete(false,"Inicio",false);
		List<Paquete> l2 = new  ArrayList<Paquete>();
		l2.add(p);
		this.setPaquetes(l2);
		
	} 
	
	public int getsitioId() {
		return this.sitioId;
	}

	public void setsitioId(int sitioId) {
		this.sitioId = sitioId;
	}   
	public String getTitulo() {
		return this.titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}   
	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}   
  
	public List<Paquete> getPaquetes() {
		return this.paquetes;
	}

	public void setPaquetes(List<Paquete> paquetes) {
		this.paquetes = paquetes;
	}   
	public List<Categoria> getCategorias() {
		return this.categorias;
	}

	public void setCategorias(List<Categoria> categorias) {
		this.categorias = categorias;
	}   
	public List<Fan> getFans() {
		return this.fans;
	}

	public void setFans(List<Fan> fans) {
		this.fans = fans;
	}  
	public List<FanPremium> getFansPremium() {
		return this.fansPremium;
	}

	public void setFansPremium(List<FanPremium> fansPremium) {
		this.fansPremium = fansPremium;
	}  
	public List<Seccion> getSecciones() {
		return this.secciones;
	}

	public void setSecciones(List<Seccion> secciones) {
		this.secciones = secciones;
	}   
	public List<String> getListaNegra() {
		return this.listaNegra;
	}

	public void setListaNegra(List<String> listaNegra) {
		this.listaNegra = listaNegra;
	}   
	public boolean getQA_activo() {
		return this.QA_activo;
	}

	public void setQA_activo(boolean QA_activo) {
		this.QA_activo = QA_activo;
	}
   
}
