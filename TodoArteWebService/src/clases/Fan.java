package clases;

import clases.Usuario;
import java.io.Serializable;
import java.lang.String;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;


public class Fan extends Usuario implements Serializable {

	
	private String pais;
	private static final long serialVersionUID = 1L;

	public Fan() {
		super();
	}   
	

	public String getPais() {
		return this.pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}   

   
}
