package clases;

import java.util.List;

public class DtPaquete {
	
	int idpaquete;
	String nombre;
	List<DtContenido> l;
	Boolean privado;
	Boolean premium;
	
	public DtPaquete(String nombre, int idpaquete,List<DtContenido> l,Boolean privado,Boolean premium ){
		this.nombre = nombre;
		this.idpaquete = idpaquete;
		this.l = l;
		this.privado = privado;
		this.premium = premium;

	} 
	
	
	public int getIdpaquete() {
		return idpaquete;
	}

	public String getNombre() {
		return nombre;
	}

	public List<DtContenido> getL() {
		return l;
	}
	public Boolean isPrivado() {
		return privado;
	}

	public Boolean isPremium() {
		return premium;
	}



	
	
	

}
