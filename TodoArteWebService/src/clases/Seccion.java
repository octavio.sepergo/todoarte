package clases;

import java.io.Serializable;
import java.lang.String;
import javax.persistence.*;


public class Seccion implements Serializable {

	   

	private int seccionId;
	private String nombre;
	private String valor;
	private static final long serialVersionUID = 1L;

	public Seccion(String nombre,String valor) {
		super();
		this.nombre =nombre;
		this.valor = valor;
	}
	public Seccion() {
		super();
	} 
	
	public int getSeccionId() {
		return this.seccionId;
	}
	
	public String getValor() {
		return valor;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}
	public void setSeccionId(int seccionId) {
		this.seccionId = seccionId;
	}   
	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
   
}
