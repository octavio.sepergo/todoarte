package clases;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;



public class Paquete implements Serializable {

	
	private static final long serialVersionUID = 1L;


	private String nombre;

	private int idPaquete;
	
	private boolean premium;	
	
	private boolean pribacidad;

	private List<Contenido> contenidos=new ArrayList<Contenido>();  
	

	public Paquete() {
		super();
	}
	
	public Paquete(boolean premium,String nombre,boolean privacidad) {
		super();
		this.nombre = nombre;
		this.premium = premium;
		this.pribacidad = privacidad;
	}


	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	  public int getIdPaquete() {
		return idPaquete;
	}
	public void setIdPaquete(int idPaquete) {
		this.idPaquete = idPaquete;
	}
	public List<Contenido> getContenidos() {
		return contenidos;
	}

	public void setContenidos(List<Contenido> contenidos) {
		this.contenidos = contenidos;
	}





	public static long getSerialversionuid() {
		return serialVersionUID;
	}



	public boolean isPremium() {
		return premium;
	}



	public void setPremium(boolean premium) {
		this.premium = premium;
	}

	public boolean isPribacidad() {
		return pribacidad;
	}

	public void setPribacidad(boolean pribacidad) {
		this.pribacidad = pribacidad;
	}





}
