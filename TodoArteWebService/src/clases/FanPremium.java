package clases;

import java.io.Serializable;
import java.lang.Integer;
import java.sql.Date;
import java.util.Calendar;

import javax.persistence.*;



public class FanPremium implements Serializable {

	   
	private Integer fanPremiunId;
	
	private String fechaInicio;
	
	private String fechaCaducidad;
	private String nick;
	
	private static final long serialVersionUID = 1L;

	public FanPremium() {
		super();
	}   
	
	public FanPremium(String nick) {
		super();
		this.nick = nick;
		fechaInicio = new Date(Calendar.getInstance().getTime().getTime()).toString();
		
		  Calendar calendar = Calendar.getInstance();
	      calendar.setTime(Calendar.getInstance().getTime()); 
	      calendar.add(Calendar.DAY_OF_YEAR,+30);  
		
		fechaCaducidad = new Date(calendar.getTime().getTime()).toString();
		
	}
	
	public Integer getFanPremiunId() {
		return this.fanPremiunId;
	}

	public void setFanPremiunId(Integer fanPremiunId) {
		this.fanPremiunId = fanPremiunId;
	}   
	public String getFechaInicio() {
		return this.fechaInicio;
	}

	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}   
	public String getFechaCaducidad() {
		return this.fechaCaducidad;
	}

	public void setFechaCaducidad(String fechaCaducidad) {
		this.fechaCaducidad = fechaCaducidad;
	}

	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}
	
	
   
}
