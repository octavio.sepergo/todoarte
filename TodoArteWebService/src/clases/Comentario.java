package clases;

import java.io.Serializable;
import java.lang.String;
import java.sql.Date;
import javax.persistence.*;



public class Comentario implements Serializable {

	private static final long serialVersionUID = 1L;

	   
	private int idComentario;
	private String texto;
	private String fecha;
	private String nick;
	
	public Comentario() {
		super();
	}   
	public int getIdComentario() {
		return this.idComentario;
	}

	public void setIdComentario(int idComentario) {
		this.idComentario = idComentario;
	}   
	public String getTexto() {
		return this.texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}   
	public String getFecha() {
		return this.fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getNick() {
		return nick;
	}
	public void setNick(String nick) {
		this.nick = nick;
	}
   
}

