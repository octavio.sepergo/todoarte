package clases;

import java.io.Serializable;
import java.lang.Integer;
import java.lang.String;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;



public class Contenido implements Serializable {

  
	private int idContenido;
	
	private String titulo;
	
	private int precio;
	
	
	private String mongo;
	

	private List<Comentario> comentarios = new ArrayList<Comentario>();  

    private List<Puntaje> Puntajes = new ArrayList<Puntaje>();
	
	private static final long serialVersionUID = 1L;

	public Contenido() {
		super();
	}   
	

	
	public Contenido(String titulo,String mongo,int precio) {
		super();
		this.titulo = titulo;
		this.mongo = mongo;
		this.precio = precio;
	}
	

	public String getTitulo() {
		return this.titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}   
  
 
	public int getIdContenido() {
		return this.idContenido;
	}

	public void setIdContenido(int idContenido) {
		this.idContenido = idContenido;
	}

	public String getMongo() {
		return mongo;
	}

	public void setMongo(String mongo) {
		this.mongo = mongo;
	}

	public List<Comentario> getComentarios() {
		return comentarios;
	}

	public void setComentarios(List<Comentario> comentarios) {
		this.comentarios = comentarios;
	}



	public int getPrecio() {
		return precio;
	}



	public void setPrecio(int precio) {
		this.precio = precio;
	}



	public List<Puntaje> getPuntajes() {
		return Puntajes;
	}



	public void setPuntajes(List<Puntaje> puntajes) {
		Puntajes = puntajes;
	}
   
	
}
