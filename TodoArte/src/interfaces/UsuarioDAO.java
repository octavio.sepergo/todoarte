package interfaces;

import clases.Artista;
import clases.Fan;
import clases.Sitio;
import clases.Categoria;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import java.util.Date;


@Path("/Usuario")
@Consumes({ "application/json" })
@Produces({ "application/json" })
public interface UsuarioDAO {

	public void programarQyA(String artista,Date fechaInicio);

	@Path("/buscarArtista")
	@POST
    public Artista buscarArtista(String artista);
	@Path("/listarArtistasUnban")
	@GET
	public List<Artista> listarArtistasUnban();
	@Path("/listarArtistasUnbanyBan")
	@GET
	public List<String> listarArtistasUnbanyBan();
	
	public String registrarArtista(Artista art);
	public void crearSitio(Sitio sitio);	
	
	public Artista buscarArtistaBan(String artista);
	@GET
	@Path("/listarCategorias")
	@Produces({ "application/json" })
	public List<String> listarCategorias();
	
    public List<Categoria> listarCategoriasAdmin();
	
    public List<Artista> listarArtistasBan();
    
	@Path("/BuscarArtistaCategoria")
	@POST
	public List<Artista> BuscarArtistaCategoria(int id);
	@Path("/BuscarArtistaRelacionBusqueda")
	@POST
	public List<Artista> BuscarArtistaRelacionBusqueda(String artista);
	public Categoria buscarCategoria(int id);
	public int cantSitio();
	
	
}
