package interfaces;

import javax.ejb.Local;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import clases.Admin;
import clases.DtLogin;
import clases.Usuario;


@Path("/Seguridad")
@Consumes({ "application/json" })
@Produces({ "application/json" })
public interface ServiciosSeguridad {
	
	@Path("/existeUsuario")
	@POST
    public Usuario existeUsuario(DtLogin data);
	public Admin existeAdmin(String nick, String pass);
}

