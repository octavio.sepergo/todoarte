package interfaces;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import clases.Contenido;
import clases.Fan;
import clases.Paquete;

@Path("/Sitio")
@Consumes({ "application/json" })
@Produces({ "application/json" })
public interface SitioDAO {
	
		@Path("/BuscarArtistaCategoria")
		@POST
		public String registrarFan(Fan entity);
		
		public List<String> listarFanesUnban();
        public List<String> listarFansBan();
		
	    public Fan buscarFan(String fan);
	    
		public void loginFan(String nick);
		
		public List<String> consultaFan(String artista);
		
		public List<String> consultarEstadistica(String nombre);/* rebisar retorno y entrada*/
		
		public List<Contenido> listarContenidoReportUnBan();
        public List<Contenido> listarContenidoBan();
		
		public void reportContenido(Integer id);
		
		public void subirContenido(Contenido entity);
		
		public void editarContenido(Contenido entity);

		public void borrarContenido(Integer id);
		
		public void crearPaquete(Paquete entity);
		
		public void agregarContenidoPaquete(Contenido entity);

		public void ingrezarQA();
		
		public void programarQA();
		
		public void banFan(String nick);
		
		public void desBanFan(String nick);
		
		
}
