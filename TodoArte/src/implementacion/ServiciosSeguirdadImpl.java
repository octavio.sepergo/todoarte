package implementacion;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import clases.Admin;
import clases.Artista;
import clases.DtLogin;
import clases.Usuario;
import interfaces.ServiciosSeguridad;
import interfaces.UsuarioDAO;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class ServiciosSeguirdadImpl implements ServiciosSeguridad{
	@EJB
	private UsuarioDAO usuarioDAO;
	private EntityManagerFactory emf = Persistence.createEntityManagerFactory("TodoArte");
	private EntityManager em = emf.createEntityManager();
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public Usuario existeUsuario(DtLogin data) { 
		String nick =data.getNick();
		String pass =data.getPass();
        Usuario us = em.find(Usuario.class, nick);
        if(us!=null) {
            if(us.getPass().equals(pass)) {
                return us;
            }
            return null;
        }
        return null;

    }
	public Boolean isSecion() {
		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		HttpSession session = request.getSession();
		return (session != null && session.getAttribute("user") != null);
	};
	
	public Admin existeAdmin(String nick, String pass) {
        Admin ad = em.find(Admin.class, nick);
        if (ad != null) {
            if (ad.getPass().equals(pass)) {
                return ad;
            }
            return null;
        }
        return null;

    }
	
}
