package implementacion;

import java.io.Serializable;

import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import clases.Usuario;
import service.ChatRoom;

@Named
@ViewScoped
public class ChatController implements Serializable {
	private static final long serialVersionUID = -2592023720528506766L;
	private String message;


	@Inject
	private ChatRoom chatRoom;

	public ChatController() {
	}

	public String getChatHistory() {
		return chatRoom.getChatHistory();
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void updateClock() {
		this.chatRoom.broadcastClock();
	}

	public void sendMessage() {
		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		HttpSession session = request.getSession();
		if (session != null && session.getAttribute("user") != null) {
			Usuario us = (Usuario) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(session.getAttribute("user"));
		chatRoom.broadcastMessage(us.getNick(), message);
		this.message = null;
		}
	}
}
