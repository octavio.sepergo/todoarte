package implementacion;

import interfaces.SitioDAO;
import interfaces.UsuarioDAO;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;
import java.util.Timer;
import java.util.TimerTask;
import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Persistence;
import javax.persistence.Query;


import clases.Artista;
import clases.Categoria;
import clases.Sitio;
import clases.Fan;

/**
 * Session Bean implementation class UsuarioDAOImpl
 */
@NamedQueries({
		@NamedQuery(name = "listarArtistasUnban", query = "SELECT u.nick FROM usuarios u WHERE u.ban = 0 AND u.TYPE = 'A'"),
		@NamedQuery(name = "listarArtistasUnbanyBan", query = "SELECT u.nick FROM usuarios u WHERE u.TYPE = 'A'") })
@Stateless
@Local(UsuarioDAO.class)
@LocalBean
public class UsuarioDAOImpl implements UsuarioDAO {
	@EJB
	private SitioDAO sitioDAO;
	@EJB
	private UsuarioDAO usuarioDAO;
	private EntityManagerFactory emf = Persistence.createEntityManagerFactory("TodoArte");
	private EntityManager em = emf.createEntityManager();

	/**
	 * Default constructor.
	 */
	public UsuarioDAOImpl() {
		// TODO Auto-generated constructor stub
	}

	public Artista buscarArtistaBan(String artista) {
        try {
            Artista us = em.find(Artista.class, artista);
            System.err.println("Aqui llega2");
            if(us!=null){
                if(us.isBan()==true) {
                    System.err.println("Aqui llega3");
                    return us;
                }else {
                    System.err.println("Aqui llega4");
                    return null;
                }
            }else {
                return null;
            }

        }catch(Exception e) {
            System.out.println("Ocurrio excepcion-->"+">>>"+e);
        }
        return null;
    }
	

	public List<String> listarArtistasUnbanyBan() {
		try {
			Query query = em.createNativeQuery("SELECT u.nick FROM usuarios u WHERE u.TYPE = 'A'");
			List<String> artistas = (List<String>) query.getResultList();
			return artistas;
		} catch (Exception e) {
			System.out.println("Ocurrio excepcion-->" + ">>>" + e);
		}
		return null;
	}


	public List<Artista> BuscarArtistaCategoria(int idcat) {
		try {

			List<Artista> artistas = (List<Artista>) em.createNativeQuery(
					"SELECT u.* FROM bdsql.usuarios u ,bdsql.sitio s WHERE u.ban = 0 AND u.TYPE = 'A' AND u.SITIO_SITIOID=s.SITIOID AND s.SITIOID IN(SELECT sitio_id FROM bdsql.sitio_categoria WHERE categoria_id = "
							+ idcat + ")",
					Artista.class).getResultList();
			System.out.println("Listado de Artistas: " + "");
			Artista arte;
			java.util.Iterator<Artista> i = artistas.iterator();
			while (i.hasNext()) {
				arte = (Artista) i.next();
				System.out.println(arte.getNick() + "");
				System.out.println(arte.getCorreo() + "");
				System.out.println("----------------" + "");
			}
			return artistas;
		} catch (Exception e) {
			System.out.println("Ocurrio excepcion-->" + ">>>" + e);
		}
		return null;
	}

	public List<Artista> BuscarArtistaRelacionBusqueda(String artista) {	
		
try {
		List<Artista> artistas = (List<Artista>) em.createNativeQuery("SELECT u.* FROM bdsql.usuarios u WHERE u.nick LIKE '%"+artista+"%' AND u.ban = 0 AND u.type = 'A'", Artista.class).getResultList(); 
		System.out.println("Listado de Artistas: "+"");
		Artista arte;
		java.util.Iterator<Artista> i = artistas.iterator();
		 while (i.hasNext()) {
		 arte = (Artista) i.next();
		 System.out.println(arte.getNick()+"");
		 System.out.println(arte.getCorreo()+"");
		 System.out.println("----------------" + "");
		 }
		return artistas; 
	}catch(Exception e) {
		System.out.println("Ocurrio excepcion-->"+">>>"+e);
	}
	return null;
}
			
		
		
	/**
     * @see UsuarioDAO#buscarArtista(String)
     */
    public Artista buscarArtista(String artista) {
			try {
				Artista us = em.find(Artista.class, artista);
				if(us.isBan()==true) {
					return null;
				}else {
					return us;
				}
			}catch(Exception e) {
				System.out.println("Ocurrio excepcion-->"+">>>"+e);
			}
			return null;
    }

    public Categoria buscarCategoria(int id) {
			try {
				Categoria cat = em.find(Categoria.class, id);
				return cat;
			}catch(Exception e) {
				System.out.println("Ocurrio excepcion-->"+">>>"+e);
			}
			return null;
    }
    
    public Boolean existeUsuario(String nick,String pass) {
    	return true;
    

    }

	/**
	 * @see UsuarioDAO#crearSitio(String)
	 */
	public void crearSitio(Sitio sitio) {
		try {
			em.getTransaction().begin();
			em.persist(sitio);
			em.getTransaction().commit();
			System.out.println("Sitio creado con exito");
		} catch (Exception e) {
			System.out.println("Ocurrio excepcion-->" + ">>>" + e);
		}
	}

	public int cantSitio() {
		try {
			Query query = em.createNativeQuery("SELECT count(*) FROM bdsql.sitio");
			int cat = (int) query.getFirstResult();
			return cat;
		} catch (Exception e) {
			System.out.println("Ocurrio excepcion-->" + ">>>" + e);
		}
		return 0;
	}

	/**
	 * @see UsuarioDAO#comprarPaquete(String)
	 */
	public String comprarPaquete(String artista) {
		// TODO Auto-generated method stub
		return null;

	}

	/**
	 * @see UsuarioDAO#serPremium()
	 */
	public String serPremium() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * @see UsuarioDAO#reportar(String)
	 */
	public String reportar(String artista) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * @see UsuarioDAO#comprarContenido(String)
	 */
	public String comprarContenido(String artista) {
		// TODO Auto-generated method stub
		return null;
	}

	public String registrarArtista(Artista art) {

		try {
			Artista us = usuarioDAO.buscarArtista(art.getNick());
			if (us != null) {
				return "Artista ya existe";
			} else {
				em.getTransaction().begin();
				em.persist(art);
				em.getTransaction().commit();
				em.clear();
				System.out.println("Artista creado con exito");
				return "Bienvenido";
			}
		} catch (Exception e) {
			System.out.println("Ocurrio excepcion-->" + ">>>" + e);
		}
		return null;
	}


	
    public void programarQyA(String artista,Date fechaInicio) {
    	
    	
    	
    	TimerTask Alerta = new TimerTask()
        {
            public void run() 
            {	
            	System.err.println("Timer");
            	Artista us = usuarioDAO.buscarArtista(artista);
        		if (us == null) {
        			System.err.println("Timer3");
        			return;
        		} else {
        			List<Fan> fan = (List<Fan>) us.getSitio().getFans();
        			ListIterator<Fan> it = fan.listIterator();
        			while(it.hasNext()) {
        				Fan f = it.next();
        				System.err.println("Timer2");
        				f.setNotificacionesNuevas(f.getNotificacionesNuevas()+1);
        				f.getNotificaciones().add(artista+" esta por Iniciar la QyA");
        				em.getTransaction().begin();
        				em.merge(f);
        				em.getTransaction().commit();
        				
        			}
        		}
          }
        };
        
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(fechaInicio); 
        calendar.add(Calendar.MINUTE, -5); 
        Date fechaSalida = calendar.getTime(); 
        Timer timer = new Timer();
        timer.schedule(Alerta, fechaSalida);
        System.err.println("Timer hora"+ fechaSalida);
        Artista us = usuarioDAO.buscarArtista(artista);
		if (us == null) {
			return;
		} else {
			if(us.getSitio() == null) {
				return;
			}else {
			if(us.getSitio().getFans() == null) {
				return;
			}
			}
		List<Fan> fan = (List<Fan>) us.getSitio().getFans();
        ListIterator<Fan> it = fan.listIterator();
        while(it.hasNext()) {
        	Fan f = it.next();
        	f.setNotificacionesNuevas(f.getNotificacionesNuevas()+1);
        	f.getNotificaciones().add(artista+" a programado una QyA para el dia"+fechaInicio.toString());
        	em.getTransaction().begin();
			em.merge(f);
			em.getTransaction().commit();
			
        }
        
    	return;
		}


}

    public List<String> listarCategorias() {
        try {
            Query query = em.createNativeQuery("SELECT c.nombre FROM categoria c");
            List<String> cat = (List<String>) query.getResultList();
            return cat;
        } catch (Exception e) {
            System.out.println("Ocurrio excepcion-->" + ">>>" + e);
        }
        return null;
    }

    public List<Categoria> listarCategoriasAdmin(){
        try {
            List<Categoria> cates = (List<Categoria>) em
                    .createNativeQuery("SELECT * FROM categoria c", Categoria.class)
                    .getResultList();
            return cates;
        } catch (Exception e) {
            System.out.println("Ocurrio excepcion-->" + ">>>" + e);
        }
        return null;
    }
    
    public List<Artista> listarArtistasUnban() {
        try {

            List<Artista> artistas = (List<Artista>) em
                    .createNativeQuery("SELECT * FROM usuarios u WHERE u.ban = 0 AND u.TYPE = 'A'", Artista.class)
                    .getResultList();

            Artista arte;
            java.util.Iterator<Artista> i = artistas.iterator();
            while (i.hasNext()) {
                arte = (Artista) i.next();
            }
            return artistas;
        } catch (Exception e) {
            System.out.println("Ocurrio excepcion-->" + ">>>" + e);
        }
        return null;
    }

    public List<Artista> listarArtistasBan() {
        try {
            List<Artista> artistas = (List<Artista>) em
                    .createNativeQuery("SELECT * FROM usuarios u WHERE u.ban = 1 AND u.TYPE = 'A'", Artista.class)
                    .getResultList();

            Artista arte;
            java.util.Iterator<Artista> i = artistas.iterator();
            while (i.hasNext()) {
                arte = (Artista) i.next();

            }
            return artistas;
        } catch (Exception e) {
            System.out.println("Ocurrio excepcion-->" + ">>>" + e);
        }
        return null;
    }
    
}
