package implementacion;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import clases.Artista;
import clases.Contenido;
import clases.Fan;
import clases.Paquete;
import clases.Sitio;
import interfaces.ServiciosSeguridad;
import interfaces.SitioDAO;
import interfaces.UsuarioDAO;

/**
 * Session Bean implementation class SitioDAOImp
 */
@Stateless
@Local(SitioDAO.class)
@LocalBean

public class SitioDAOImp implements SitioDAO {


	private EntityManagerFactory emf = Persistence.createEntityManagerFactory("TodoArte");
	private EntityManager em = emf.createEntityManager();
    
	 @EJB
     private SitioDAO sitioDAO; 
	 @EJB
     private ServiciosSeguridad ss; 
	/**
     * 
     * Default constructor. 
     */
    public SitioDAOImp() {
        // TODO Auto-generated constructor stub
    }
    

    public String registrarFan(Fan entity) {
    	
		try {


				em.getTransaction().begin();
				em.persist(entity);
				em.getTransaction().commit();
				System.out.println("Fan creado con exito");
				return "Nuevo Fan registrado";
			
		}catch(Exception e) {
			System.out.println("Ocurrio excepcion-->"+">>>"+e);
		}
		return null; 	
    }
    
    
	/**
     * @see UsuarioDAO#buscarArtista(String)
     */
    public Fan buscarFan(String fan) {
			try {
				Fan us = em.find(Fan.class, fan);
				if(us!=null) {
					return us;
				}else {
					return null;
				}
			}catch(Exception e) {
				System.out.println("Ocurrio excepcion-->"+">>>"+e);
			}
			return null;
    }
	public void loginFan(String nick) {
		// TODO Auto-generated method stub
		
	}
	public void consultaFan() {
		// TODO Auto-generated method stub
		
	}
	public List<String> consultarEstadistica(String nombre) {
		// TODO Auto-generated method stub
		return null;
	}
	public void reportContenido(Integer id) {
		// TODO Auto-generated method stub
		
	}
	public void subirContenido(Contenido entity) {
		// TODO Auto-generated method stub
		
	}
	public void editarContenido(Contenido entity) {
		// TODO Auto-generated method stub
		
	}
	public void borrarContenido(Integer id) {
		// TODO Auto-generated method stub
		
	}
	public void crearPaquete(Paquete entity) {
		// TODO Auto-generated method stub
		
	}
	public void agregarContenidoPaquete(Contenido entity) {
		// TODO Auto-generated method stub
		
	}
	public void ingrezarQA() {
		// TODO Auto-generated method stub
		
	}
	public void programarQA() {
		// TODO Auto-generated method stub
		
	}
	public void banFan(String nick) {
		// TODO Auto-generated method stub
		
	}
	public void desBanFan(String nick) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public List<String> consultaFan(String artista) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public List<String> listarFansBan() {

        try {
            Query query = em.createNativeQuery("SELECT listanegra FROM bdsql.sitio_listanegra;");
            List<String> cat = (List<String>) query.getResultList();
            return cat;
        } catch (Exception e) {
            System.out.println("Ocurrio excepcion-->" + ">>>" + e);
        }
        return null;
    }

    public List<String> listarFanesUnban() {
        try {
            Query query = em.createNativeQuery("SELECT fans FROM bdsql.sitio_fans;");
            List<String> cat = (List<String>) query.getResultList();
            return cat;
        } catch (Exception e) {
            System.out.println("Ocurrio excepcion-->" + ">>>" + e);
        }
        return null;

    }
    
    public List<Contenido> listarContenidoReportUnBan(){
        List<Contenido> conts = (List<Contenido>) em.createNativeQuery(
                "SELECT c.* FROM bdsql.contenido c WHERE  c.ban = 0 AND  c.idContenido in ( select r.Contenido_idContenido from  bdsql.contenido_reportes r)",
                Contenido.class).getResultList();

        return conts;
    }
    public List<Contenido> listarContenidoBan(){
        List<Contenido> conts = (List<Contenido>) em.createNativeQuery(
                "SELECT c.* FROM bdsql.contenido c WHERE  c.ban = 1",
                Contenido.class).getResultList();

        return conts;
    }
}
