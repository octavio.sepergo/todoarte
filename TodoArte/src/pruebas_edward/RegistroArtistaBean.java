package pruebas_edward;

import java.io.Serializable;


import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.primefaces.PrimeFaces;

import clases.Artista;
import clases.Categoria;
import clases.Fan;
import clases.Sitio;
import clases.Usuario;
import interfaces.SitioDAO;
import interfaces.UsuarioDAO;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
@Named(value = "RegistroArtistaBean")
@SessionScoped

@TransactionManagement(TransactionManagementType.BEAN)
public class RegistroArtistaBean implements Serializable{
	
	/**
	 * 
	 */
	private EntityManagerFactory emf = Persistence.createEntityManagerFactory("TodoArte");
	private EntityManager em = emf.createEntityManager();
	
	private static final long serialVersionUID = 1L;
	@EJB
	private UsuarioDAO usuarioDAO;
	@EJB
	private SitioDAO sitioDAO;
	private String nick;
	private String pass;
	private String correo;
	private String direccion;
	private String nombre;
	private String sexoEleg;
	private Date fecha;
	private String titulo;
	private String des;
	
	private String catFav;
	private List<String> catFavItems;
	private List<SelectItem> listaS;
	private List<SelectItem> catItem;
	
	private String msj= "Enorabuena por registrarte";
	@PostConstruct
	public void init() {
		//lista de colores utilizando la classe SelecItem
		listaS = new ArrayList<SelectItem>();
		listaS.add(new SelectItem(1, "Hombre"));
		listaS.add(new SelectItem(2, "Mujer"));
		List<String> cat = usuarioDAO.listarCategorias();
		catItem = new ArrayList<SelectItem>();
		int cont = 0;
		for(String str : cat)
		{
			cont++;
			catItem.add(new SelectItem(cont, str)); 
		}
	}
	
 
	public RegistroArtistaBean() {
		// TODO Auto-generated constructor stub
	}
	

	public Date getFecha() {
		return fecha;
	}


	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}


	public UsuarioDAO getUsuarioDAO() {
		return usuarioDAO;
	}

	public void setUsuarioDAO(UsuarioDAO usuarioDAO) {
		this.usuarioDAO = usuarioDAO;
	}

	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getSexoEleg() {
		return sexoEleg;
	}

	public void setSexoEleg(String sexoEleg) {
		this.sexoEleg = sexoEleg;
	}

	public List<SelectItem> getListaS() {
		return listaS;
	}

	public void setListaS(List<SelectItem> listaS) {
		this.listaS = listaS;
	}
	
	
	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getDes() {
		return des;
	}

	public void setDes(String des) {
		this.des = des;
	}

	
	public String getCatFav() {
		return catFav;
	}

	public void setCatFav(String catFav) {
		this.catFav = catFav;
	}

	public List<SelectItem> getCatItem() {
		return catItem;
	}

	public void setCatItem(List<SelectItem> catItem) {
		this.catItem = catItem;
	}
	
	

	public String getMsj() {
		return msj;
	}

	public void setMsj(String msj) {
		this.msj = msj;
	}

	public List<String> getCatFavItems() {
		return catFavItems;
	}

	public void setCatFavItems(List<String> catFavItems) {
		this.catFavItems = catFavItems;
	}

	public void registrar() {
		
		FacesMessage message = null;
        boolean loggedIn = false;
		Usuario us= em.find(Usuario.class,nick);
		if(us == null) {
		Artista a5 = new Artista();
		a5.setNick(nick);
		a5.setPass(pass);
		a5.setCorreo(correo);
		a5.setNombre(nombre);
		a5.setDireccion(direccion);
		a5.setBan(false);
		java.sql.Date sDate = new java.sql.Date(fecha.getTime());
		a5.setFechaNacimiento(sDate);
		int foo2 = Integer.parseInt(sexoEleg);
		if(foo2==1){
			a5.setSexo("H");
		}else {
			a5.setSexo("M");
		}
		a5.setSaldo(35000);
		
		Sitio s = new Sitio("Descripcion","Titulo");
		a5.setSitio(s);
		em.getTransaction().begin();
		em.persist(s);
		em.getTransaction().commit();
		
		em.getTransaction().begin();
		em.persist(a5);
		em.getTransaction().commit();
		em.clear();
		
		
		us= em.find(Usuario.class,nick);
		FacesContext context = FacesContext.getCurrentInstance();  
		context.getExternalContext().getSessionMap().put("user", us); 
		loggedIn = true;
        message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Bienvenido", nick);
        FacesContext.getCurrentInstance().addMessage(null, message);
        PrimeFaces.current().ajax().addCallbackParam("loggedIn", loggedIn);

	}
		else {
			message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Registro Error", "Nick ya esta en uso");

			loggedIn = false;
			FacesContext.getCurrentInstance().addMessage(null, message);
			PrimeFaces.current().ajax().addCallbackParam("loggedIn", loggedIn);
			
		}	
	}

}
