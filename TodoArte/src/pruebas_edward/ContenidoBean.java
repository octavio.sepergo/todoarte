package pruebas_edward;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.annotation.ManagedBean;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.faces.annotation.ManagedProperty;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.faces.event.ActionListener;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import org.bson.types.ObjectId;
import org.primefaces.PrimeFaces;
import org.primefaces.component.fileupload.FileUpload;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import com.mongodb.DB;
import com.mongodb.MongoClient;
import com.mongodb.gridfs.GridFS;
import com.mongodb.gridfs.GridFSDBFile;
import com.mongodb.gridfs.GridFSInputFile;

import clases.Artista;
import clases.Comentario;
import clases.Contenido;
import clases.DtComentario;
import clases.Paquete;
import clases.Puntaje;
import clases.Usuario;

@Named(value = "ContenidoBean")
@ViewScoped

public class ContenidoBean implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int precio;
	private String titulo;
	private String paquete;
	private Part archivo;
	private Contenido con;
	private Paquete pac;
	
	@ManagedProperty(value="#{param.id}") 
	private int idC; 
	
	private Integer ppp;
    private String nick;
    private int punt;
    private int idContenido;
    private List<DtComentario> lComentarios = new ArrayList<DtComentario>();;
    private String textoComentario;
    private Puntaje pun;

	
	private EntityManagerFactory emf = Persistence.createEntityManagerFactory("TodoArte");
	private EntityManager em = emf.createEntityManager();
	

	
	public ContenidoBean() {		
		
	}

	public String subirContenido(String nick) throws IOException {

		  FacesMessage message = null;

	       boolean loggedIn = false;
		
		
		
		MongoClient mon = new MongoClient("localhost");
		DB database = mon.getDB("bdmong");
		GridFS gfsArchivo = new GridFS(database);
		InputStream in;

		in = archivo.getInputStream();
		byte[] buff = new byte[(int) archivo.getSize()];
		in.read(buff);
		in.close();

        
		GridFSInputFile gfs = gfsArchivo.createFile(buff);
		String str = archivo.getContentType();

		gfs.setContentType(str);
		gfs.save(); 
		String mong = gfs.getId().toString();
		
	
		this.setCon(new Contenido(titulo,mong,precio));
		em.getTransaction().begin();
		pac = em.find(Paquete.class, Integer.parseInt(paquete));
		pac.getContenidos().add(con);
		em.merge(pac);
		em.getTransaction().commit();
		
		mon.close();
		
		
		loggedIn = true;
        message = new FacesMessage(FacesMessage.SEVERITY_INFO, "contenido Subido Correctamente"," " );
        FacesContext.getCurrentInstance().addMessage(null, message);
        PrimeFaces.current().ajax().addCallbackParam("loggedIn", loggedIn);
        return "sitio.xhtml?aNick="+nick+"&faces-redirect=true";


	}
	
	
	
	
	/*
	public void Config() {
		FacesMessage message = null;
        boolean loggedIn = false;
		//Seccion s1 = new Seccion("colorBody",colorBody);
		//s.getSecciones().add(s1); 
		em.getTransaction().begin();
	//	em.merge(s);
		em.getTransaction().commit();
		loggedIn = true;
        message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Configuracion Exitosa", nick);
        FacesContext.getCurrentInstance().addMessage(null, message);
        PrimeFaces.current().ajax().addCallbackParam("loggedIn", loggedIn);
 
        
	}*/


	public void comentar() {
	    HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
	    HttpSession session = request.getSession();
	    Usuario us = (Usuario) session.getAttribute("user");
	    String nick = us.getNick();

	    Comentario coment = new Comentario();
	    coment.setNick(nick);
	    coment.setTexto(textoComentario);
	    long millis=System.currentTimeMillis();
	    java.sql.Date date = new java.sql.Date(millis);
	    coment.setFecha(date);

	    em.getTransaction().begin();
	        em.persist(coment);
	        con.getComentarios().add(coment);
	        em.merge(con);
	    em.getTransaction().commit();
	    this.textoComentario = "";
	    this.idC = con.getIdContenido();
	    this.ListarComentarios();

	}
private int number;

public int getNumber() {
    return number;
}

public void increment() {
    number++;
}
	
	
public void ListarComentarios() {
	int idc;
	if(this.idC == 0) {
	FacesContext fc = FacesContext.getCurrentInstance();
    Map<String,String> params = 
       fc.getExternalContext().getRequestParameterMap();
    idc = Integer.valueOf(params.get("id")) ; 
	}
	else {
	idc = this.idC;
	}
	System.out.println("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
	System.out.println(idc);
	System.out.println("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
    con = em.find(Contenido.class,idc);
    lComentarios.clear();
    if(con.getComentarios() != null) {
        for (Comentario Com : con.getComentarios()) {
            DtComentario dt = new DtComentario(Com.getNick(),Com.getTexto(),Com.getFecha());
            lComentarios.add(dt);
        }

        Collections.reverse(lComentarios);
        for (DtComentario dtComentario : lComentarios) {
            dtComentario.getTexto();
        }
    }
    HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
    HttpSession session = request.getSession();
    Usuario us = (Usuario) session.getAttribute("user");
    punt = 0;
    if(us == null) {
    	return ;
    }
    String nick = us.getNick();
    if(con.getPuntajes() != null) {
    List<Puntaje> lp = con.getPuntajes();
        for (Puntaje puntaje : lp) {
            if(puntaje.getNick().equals(nick)) {
                punt = puntaje.getPuntaje();
            }

        }
    }
    this.idC = 0;

}
	
	
	public void puntuar() {
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        HttpSession session = request.getSession();
        Usuario us = (Usuario) session.getAttribute("user");

        String nick = us.getNick();

        List<Puntaje> lp = con.getPuntajes();
        for (Puntaje puntaje : lp) {
            if(puntaje.getNick().equals(nick)) {
                pun = puntaje;
            }
        }

        if(pun == null) {
            pun = new Puntaje(nick,punt);
            em.getTransaction().begin();
                em.persist(pun);
                con.getPuntajes().add(pun);
                em.merge(con);
            em.getTransaction().commit();
        }
        else {
            pun.setPuntaje(punt);
            em.getTransaction().begin();
            em.merge(pun);
            em.getTransaction().commit();
        }
    }
	
	public int getPrecio() {
		return precio;
	}



	public void setPrecio(int precio) {
		this.precio = precio;
	}



	public String getTitulo() {
		return titulo;
	}



	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}



	public String getPaquete() {
		return paquete;
	}



	public void setPaquete(String paquete) {
		this.paquete = paquete;
	}



	

	public Contenido getCon() {
		return con;
	}

	public void setCon(Contenido con) {
		this.con = con;
	}

	public Paquete getPac() {
		return pac;
	}

	public void setPac(Paquete pac) {
		this.pac = pac;
	}

	public Part getArchivo() {
		return archivo;
	}

	public void setArchivo(Part archivo) {
		this.archivo = archivo;
	}



	public String zx() throws IOException, URISyntaxException {
        MongoClient mon = new MongoClient("localhost");
        DB database = mon.getDB("bdmong");
        GridFS gfsArchivo = new GridFS(database);
        ObjectId iwd = new ObjectId("5ed5da0dc6919c004f166fa1");
        GridFSDBFile out = gfsArchivo.find(iwd);
        String na = out.getContentType().substring(out.getContentType().lastIndexOf ("/") + 1);
        File temp = File.createTempFile("archivo",na+".jpg", new File("C:/archivos"));
        out.writeTo(temp);
        mon.close();
        System.out.println(temp.getName());
        return temp.getName();
    }

	public Integer getPpp() {
		return ppp;
	}

	public void setPpp(Integer ppp) {
		this.ppp = ppp;
	}

	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}

	public int getPunt() {
		return punt;
	}

	public void setPunt(int punt) {
		this.punt = punt;
	}

	public int getIdContenido() {
		return idContenido;
	}

	public void setIdContenido(int idContenido) {
		this.idContenido = idContenido;
	}

	public List<DtComentario> getlComentarios() {
		return lComentarios;
	}

	public void setlComentarios(List<DtComentario> lComentarios) {
		this.lComentarios = lComentarios;
	}

	public String getTextoComentario() {
		return textoComentario;
	}

	public void setTextoComentario(String textoComentario) {
		this.textoComentario = textoComentario;
	}

	public Puntaje getPun() {
		return pun;
	}

	public void setPun(Puntaje pun) {
		this.pun = pun;
	}
	public void editarContenido() {

        em.getTransaction().begin();
            em.merge(con);
        em.getTransaction().commit();

    }

    public void prepararEditar() {
    	System.out.println("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaddddddddddddddddddddddddddddddd");
    	int id;
    	if(this.idC == 0) {
    	FacesContext fc = FacesContext.getCurrentInstance();
        Map<String,String> params = 
           fc.getExternalContext().getRequestParameterMap();
        id = Integer.valueOf(params.get("id3")) ; 
    	}
    	else {
    	id = this.idC;
    	}
    	
    	
        con = em.find(Contenido.class, id);
    }
    
    public void prepararEditar2() {
    	System.out.println("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaddddddddddddddddddddddddddddddd");
    	int id;
    	if(this.idC == 0) {
    	FacesContext fc = FacesContext.getCurrentInstance();
        Map<String,String> params = 
           fc.getExternalContext().getRequestParameterMap();
        System.out.println(params.get("id2"));
        id = Integer.valueOf(params.get("id2")) ; 
    	}
    	else {
    	id = this.idC;
    	}
    	System.out.println(id);
    	System.out.println("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaddddddddddddddddddddddddddddddd");
        con = em.find(Contenido.class, id);
    }
	
    public void eliminarContenido() {
        em.getTransaction().begin();
        	Contenido con2 = em.find(Contenido.class, con.getIdContenido());
            String idm = con2.getMongo();
            em.remove(con2);
        em.getTransaction().commit();
        em.clear();
        MongoClient mon = new MongoClient("localhost");
        DB database = mon.getDB("bdmong");
        GridFS gfsArchivo = new GridFS(database);
        ObjectId iwd = new ObjectId(idm);
        gfsArchivo.remove(iwd);

    }

    public Boolean isPaPre() {

    	 FacesContext fc = FacesContext.getCurrentInstance();
         Map<String,String> params = 
            fc.getExternalContext().getRequestParameterMap();
         String param = params.get("nickS") ;
    	 
    	 Artista a = em.find(Artista.class,param);
    	 if(paquete == null) {
    		 return false;
    	 }
    	 List<Paquete> p = a.getSitio().getPaquetes();
    	 for (Paquete pa : p) {
			if (pa.getIdPaquete() == Integer.parseInt(paquete)) {
				if(pa.isPremium()) {
					return true;
				}
			}
		}
    	 return false;
	}
   
	public int getIdC() {
		return idC;
	}

	public void setIdC(int idC) {
		this.idC = idC;
	}
	
}