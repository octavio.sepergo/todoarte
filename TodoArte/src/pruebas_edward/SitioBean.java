package pruebas_edward;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.annotation.ManagedProperty;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.faces.push.Push;
import javax.faces.push.PushContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;

import javax.servlet.http.HttpSession;

import org.bson.types.ObjectId;
import org.primefaces.PrimeFaces;
import org.primefaces.event.FlowEvent;

import com.mongodb.DB;
import com.mongodb.MongoClient;
import com.mongodb.gridfs.GridFS;
import com.mongodb.gridfs.GridFSDBFile;
import com.sun.xml.xsom.impl.parser.Patch;

import clases.Artista;
import clases.Categoria;
import clases.Chat;
import clases.Comprar;
import clases.Contenido;
import clases.DtContenido;
import clases.DtLogin;
import clases.DtPaquete;
import clases.Fan;
import clases.FanPremium;
import clases.Mensaje;
import clases.Paquete;
import clases.Sitio;
import clases.Usuario;
import clases.mensualidad;
import interfaces.ServiciosSeguridad;
import interfaces.UsuarioDAO;
import clases.Seccion;
@Named(value = "SitioBean")
@ViewScoped
public class SitioBean implements Serializable{
	
	/**
	 * 
	 */
	@EJB
	private UsuarioDAO usuarioDAO;
	private static final long serialVersionUID = 1L;
	private Artista a;
	private Sitio s;
	private String id;
	private String catP;
	private String cat;
	private ChatBean chat;
	private String tex;
	private String op;
	private String opT;
	private List<String> Img;
	private List<Seccion> secciones;
	private List<Paquete> paquetes;
	private List<DtPaquete> p;
	
	private int number;
	

	private String nombrePaq;
	private Boolean prem;
	private Boolean priv;
	private Boolean rend;
	  
	private boolean skip;
	private List<SelectItem> tarjItem;
	private String tarjFav;
	private String numeroTarj;
	private String fechaTarj;
	private String codigoTarj;
	private String telTarj;
	private String msj2;
	private String tarjetaa;
	private int idPaquete;
	private int idContenido;
	private Double saldo = 0.0;
	private int monto = 0;
	private String tipo;
	private List<Integer> con;
	private List<Integer> monto2;
	private String FechaCaducidad; 
	private String FechaCaducidad2; 
	private List<String> reportes;
    private String reportFav;
	
	private String msj = "Bienvenido a mi sitio aqui encontra contenido de calidad.. y mas..";
	private String header;
	private String titulo = "Titulo del Sitio";
	private String des = "Esto sera una descripcion de sitio";
	private String colorNavbar = "navbar navbar-dark bg-primary";
	private String colorBody = "#343131";
	private String imgUser = "../img2/user-img-default.png";
	private Boolean lateralBar;
	private List<String> im;
	private String nick = "Nick";
	
	private int idrep = 0;
	
	private EntityManagerFactory emf = Persistence.createEntityManagerFactory("TodoArte");
	private EntityManager em = emf.createEntityManager();
	
	@Inject
	@Push(channel = "updateMS")
	private PushContext updateMS;
	
	
	@PostConstruct
	public void init()  {
		
		chat = ChatBean.getIntancia();
		String param=FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("aNick");
		if(param == null) {
			try {
				
				FacesContext.getCurrentInstance().getExternalContext().redirect("/TodoArte/");
			} catch (IOException e) {

				e.printStackTrace();
			}
		}
		this.a = em.find(Artista.class,param);
		if(this.a == null) {
			try {
				
				FacesContext.getCurrentInstance().getExternalContext().redirect("/TodoArte/");
			} catch (IOException e) {

				e.printStackTrace();
			}
		}
		if(this.a.isBan()) {
			try {
				
				FacesContext.getCurrentInstance().getExternalContext().redirect("/TodoArte/");
			} catch (IOException e) {

				e.printStackTrace();
			}
		}

		
		
		this.s = a.getSitio();
		int j= s.getsitioId();
		this.id = String.valueOf(j); 
	
		
		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		HttpSession session = request.getSession();
		if (session != null && session.getAttribute("user") != null) {
			Usuario us = (Usuario) session.getAttribute("user");
			this.nick = us.getNick();
			List<String> ln = s.getListaNegra();
			if(ln.contains(this.nick)) {
				try {
					FacesContext context = FacesContext.getCurrentInstance();
			        context.getExternalContext().getFlash().setKeepMessages(true);
			        context.addMessage("msg", new FacesMessage("Usted Esta Bloqueado en este sitio"));
			        FacesContext.getCurrentInstance().getExternalContext().redirect("/TodoArte/");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(this.isSecionArtista()) {
				if(us.getNick().equals(a.getNick())) {
					
				}
				else {
				try {
					FacesContext.getCurrentInstance().getExternalContext().redirect("/TodoArte/template/sitio.xhtml?aNick="+us.getNick());
				} catch (IOException e) {

					e.printStackTrace();
				}
				}
			}
			
		};
		
		
		

		Sitio s2 = s;
		List<Seccion> sec = s2.getSecciones();
		if(sec.size()==0 || sec == null) {
			this.colorBody = "343131";
			this.catP = "Todo";
			this.op = "op1";
			this.opT = "Times New Roman";
		}
		

		for (Seccion se : sec) {	

			if("colorBody".equals(se.getNombre())) {
				this.colorBody = se.getValor();
				break;
			}
			else {
				this.colorBody = "343131";
			};
		};
		for (Seccion se2 : sec) {	

			if("CategoriaPrincipal".equals(se2.getNombre())) {
				this.catP = se2.getValor();
				this.catPsel();
				break;
			}
			else {
				this.catP = "Imagenes";
			};
		};
		for (Seccion se2 : sec) {	

			if("VerContenido".equals(se2.getNombre())) {
				this.op = se2.getValor();
				break;
			}
			else {
				this.op = "op1";
			};
		};
		for (Seccion se2 : sec) {	
			System.out.println(se2.getNombre());
			if("FuenteTitulo".equals(se2.getNombre())) {
				this.opT = se2.getValor();
				break;
			}
			else {
				System.out.println("F");
				this.opT = "Times New Roman";
			};
		};
		this.catPsel();
		this.titulo = s.getTitulo();
		this.des = s.getDescripcion();
		this.setPaquetes(s.getPaquetes());
		
		tarjItem = new ArrayList<SelectItem>();
        tarjItem.add(new SelectItem(1, "Visa")); 
        tarjItem.add(new SelectItem(2, "MasterCard")); 
        tarjItem.add(new SelectItem(3, "Oca")); 
        
        reportes = new ArrayList<String>();
        reportes.add("Nombre Inapropiado");
        reportes.add("Contenido Inapropiado");
        reportes.add("Contenido Violento");
        reportes.add("Nombre Rasista");
        reportes.add("Contenido Rasista");
        reportes.add("Contenido malicioso");


        this.rend = false;
        
		if (session != null && session.getAttribute("user") != null) {
			Usuario us = (Usuario) session.getAttribute("user");
			this.saldo =  em.find(Usuario.class,us.getNick()).getSaldo();
			 
		}
		
		if (session != null && session.getAttribute("user") != null) {
			Usuario us = (Usuario) session.getAttribute("user");
			List<FanPremium> fa2 = s.getFansPremium();
			
			for (FanPremium aux : fa2) {
				
				if (aux.getNick().equals(us.getNick())) {
					
					Date d = aux.getFechaCaducidad();
					DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
					String strDate = dateFormat.format(d);
					this.FechaCaducidad = strDate;

				}
			}
			}
		
			if (session != null && session.getAttribute("user") != null) {
				Usuario us = (Usuario) session.getAttribute("user");
				List<mensualidad> fa22 = (List<mensualidad>) em.createNativeQuery("SELECT * FROM mensualidad", mensualidad.class).getResultList();
				System.out.println("2222222222222");
				System.out.println(fa22);
				System.out.println("2222222222222");
				for (mensualidad aux2 : fa22) {
					System.out.println("2212222122222212");
					if (aux2.getNick().equals(us.getNick())) {
						System.out.println(aux2.getFechaCaducidad());
						Date d = aux2.getFechaCaducidad();
						DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
						String strDate = dateFormat.format(d);
						this.FechaCaducidad2 = strDate;
	
					}
				}
				}
	}
	
	public SitioBean() {		
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	public Artista getA() {
		return a;
	}

	public void setA(Artista a) {
		this.a = a;
	}

	public Sitio getS() {
		return s;
	}

	public void setS(Sitio s) {
		this.s = s;
	}

	public List<Seccion> getSecciones() {
		return secciones;
	}

	public void setSecciones(List<Seccion> secciones) {
		this.secciones = secciones;
	}

	public String getMsj() {
		return msj;
	}

	public void setMsj(String msj) {
		this.msj = msj;
	}

	public String getHeader() {
		return header;
	}

	public void setHeader(String header) {
		this.header = header;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getDes() {
		return des;
	}

	public void setDes(String des) {
		this.des = des;
	}

	public String getColorNavbar() {
		return colorNavbar;
	}

	public void setColorNavbar(String colorNavbar) {
		this.colorNavbar = colorNavbar;
	}

	public String getColorBody() {
		return colorBody;
	}

	public void setColorBody(String colorBody) {
		this.colorBody = colorBody;
	}

	public Boolean getLateralBar() {
		return lateralBar;
	}

	public void setLateralBar(Boolean lateralBar) {
		this.lateralBar = lateralBar;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getImgUser() {
		return imgUser;
	}

	public void setImgUser(String imgUser) {
		this.imgUser = imgUser;
	}

	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}
	
	public void Config() {
		FacesMessage message = null;
        boolean loggedIn = false;
        
        List<Seccion> sec = s.getSecciones();
       int aux =1;
       int aux2 =1;
       int aux3 =1;
       int aux4 =1;
		for (Seccion se : sec) {	

			if("colorBody".equals(se.getNombre())) {
				se.setValor(colorBody);
				em.getTransaction().begin();
				em.merge(se);
				em.getTransaction().commit();
				aux =2;
				break;
			}
			
		};
		if(aux == 1) {
			Seccion s1 = new Seccion("colorBody",colorBody);
			s.getSecciones().add(s1);
		}
		
		for (Seccion se2 : sec) {	

			if("CategoriaPrincipal".equals(se2.getNombre())) {
				se2.setValor(catP);
				em.getTransaction().begin();
				em.merge(se2);
				em.getTransaction().commit();
				aux2 =2;
				break;
			}

		};
		if(aux2 == 1) {
		Seccion s2 = new Seccion("CategoriaPrincipal",catP);
		s.getSecciones().add(s2); 
		}
		
		for (Seccion se2 : sec) {	

			if("VerContenido".equals(se2.getNombre())) {
				if(catP.equals("Musica")) {
					if(op.equals("op1")) {
						this.op = "op3";
					}
					else {
						this.op = "op4";
					}
					

				}
				if(catP.equals("Videos") ) {
					if(op.equals("op1")) {
						this.op = "op5";
					}
					else {
						this.op = "op6";
					}

				}
				se2.setValor(op);
				em.getTransaction().begin();
				em.merge(se2);
				em.getTransaction().commit();
				aux3 =2;
				break;
			}

		};
		
		if(aux3 == 1) {
			if(catP.equals("Musica")) {
				if(op.equals("op1")) {
					this.op = "op3";
				}
				else {
					this.op = "op4";
				}
				

			}
			if(catP.equals("Videos") ) {
				if(op.equals("op1")) {
					this.op = "op5";
				}
				else {
					this.op = "op6";
				}

			}	
		Seccion s3 = new Seccion("VerContenido",op);
		s.getSecciones().add(s3);
		}
		
		for (Seccion se2 : sec) {	
			System.out.println(se2.getNombre());
			if("FuenteTitulo".equals(se2.getNombre())) {
				se2.setValor(opT);
				em.getTransaction().begin();
				em.merge(se2);
				em.getTransaction().commit();
				aux4 =2;
				break;
			}

		};
		if(aux4 == 1) {
		Seccion s4 = new Seccion("FuenteTitulo",opT);
		s.getSecciones().add(s4); 
		}
		int opt = 2;
		if(catP.equals("Imagenes")) {
			opt=2;
			
		}
		if(catP.equals("Musica")) {
			opt=3;
			
		}
		if(catP.equals("Videos")) {
			opt=4;
			
		}
		
		if(this.isConfigTipo()) {
		Categoria c = Persistence.createEntityManagerFactory("TodoArte").createEntityManager().find(Categoria.class, opt);
		s.getCategorias().add(c);
		
		Categoria c2 = Persistence.createEntityManagerFactory("TodoArte").createEntityManager().find(Categoria.class, Integer.parseInt(cat));
		s.getCategorias().add(c2);
		}
		
		s.setTitulo(titulo);
		s.setDescripcion(des);
		
		em.getTransaction().begin();
		em.merge(s);
		em.getTransaction().commit();
		loggedIn = true;
        message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Configuracion Exitosa", nick);
        FacesContext.getCurrentInstance().addMessage(null, message);
        PrimeFaces.current().ajax().addCallbackParam("loggedIn", loggedIn);
        
        
	}

	public String getCatP() {
		return catP;
	}

	public void setCatP(String catP) {
		this.catP = catP;
	}

	public String getOp() {
		return op;
	}

	public void setOp(String op) {
		this.op = op;
	}

	public String getOpT() {
		return opT;
	}

	public void setOpT(String opT) {
		this.opT = opT;
	}
	
	
	public void catPsel() {
		List<String> img = new ArrayList<String>();
		List<String> aud = new ArrayList<String>();
		List<String> vid = new ArrayList<String>();
		String e="op1.jpg";
		String e2="op2.jpg";
		String e3="op3.jpg";
		String e4="op4.jpg";
		String e5="op5.jpg";
		String e6="op6.jpg";

		img.add(e);
		img.add(e2);
		aud.add(e3);
		aud.add(e4);
		vid.add(e5);
		vid.add(e6); 
		
		if(catP.equals("Imagenes")) {
			this.Img = img;
			return;
		}
		if(catP.equals("Musica")) {
			this.Img = aud;
			return;
		}
		if(catP.equals("Videos") ) {
			this.Img = vid;
			return;
		}

		
		
	}

	public List<String> getImg() {
		return Img;
	}

	public void setImg(List<String> img) {
		Img = img;
	}
	
	public void cargar() {

			MongoClient mon = new MongoClient("localhost");
        	DB database = mon.getDB("bdmong");
        	GridFS gfsArchivo = new GridFS(database);
			List<DtPaquete> im2 = new ArrayList<DtPaquete>();
			String na = ".jpg";
			if(this.op.equals("op1") || this.op.equals("op2")) {
				na = ".jpg";
			}
			if(this.op.equals("op3") || this.op.equals("op4")) {
				na = ".mp3";
			}
			if(this.op.equals("op5")  || this.op.equals("op6")) {
				na = ".mp4";
			}
			
			String param=FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("aNick");
			if(param == null) {
				try {
					
					FacesContext.getCurrentInstance().getExternalContext().redirect("principalindex.xhtml");
				} catch (IOException e) {

					e.printStackTrace();
				}
			}
			this.a = em.find(Artista.class,param);
			if(this.a == null) {
				try {
					
					FacesContext.getCurrentInstance().getExternalContext().redirect("principalindex.xhtml");
				} catch (IOException e) {

					e.printStackTrace();
				}
			}
			
			
			this.s = a.getSitio();
			
        	
        	for (Paquete p : s.getPaquetes()) {
        		List<DtContenido> im = new ArrayList<DtContenido>();
        		for (Contenido c : p.getContenidos()) {
        			if(!c.isBan()) {
        			 System.out.println(c.getMongo());
        	        ObjectId iwd = new ObjectId(c.getMongo());
        	        GridFSDBFile out = gfsArchivo.find(iwd);
        	       
        	        File temp;
					try {
						if(out != null) {
						temp = File.createTempFile("archivo",na, new File("C:/archivos"));
						out.writeTo(temp);
						DtContenido auxC = new DtContenido(c.getIdContenido(),temp.getName());
						im.add(auxC);
						}
            		
					} catch (IOException e) {
						System.out.println("EXPLOTO");

					}
        			}
    				
    			}
        		DtPaquete pa = new DtPaquete(p.getNombre(),p.getIdPaquete(),im,p.isPribacidad(),p.isPremium());
        		im2.add(pa);

			}
        	mon.close();
        	this.p = im2;
        	

    	}
	

		
		public List<String> getIm() 
		{
		return im;
		}



		public void setIm(List<String> im) 
		{
        this.im = im;
		}
		
		
		public Boolean isSecionArtistaDueño() {
			HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
			HttpSession session = request.getSession();
			if (session != null && session.getAttribute("user") != null) {
				Usuario us = (Usuario) session.getAttribute("user");
				if (us instanceof Artista) {
					String n = us.getNick();
					if(n.equals(a.getNick())) {
					  return true;
					  }
					else{
						  return false;
						}

					}
				else {
					  return false;
					}


			}
			 return false;

		};
		
		public Boolean isConfigTipo() {
			if(s.getCategorias().size() > 1 ) {
				return false;
				
			}
			else {
				return true;
			}

		};
		
		public List<SelectItem> liscat() {
		List<Categoria> cat = (List<Categoria>) em.createNativeQuery("SELECT * FROM Categoria", Categoria.class).getResultList();	
		List<SelectItem> catItem = new ArrayList<SelectItem>();

        for(Categoria str : cat)
        {
        	if(!str.getPrincipal()) {
            catItem.add(new SelectItem(str.getCategoriaId(), str.getNombre())); 
        	}
        }
        return catItem;
		}

		public String getCat() {
			return cat;
		}

		public void setCat(String cat) {
			this.cat = cat;
		}

		public List<Paquete> getPaquetes() {
			return paquetes;
		}

		public void setPaquetes(List<Paquete> paquetes) {
			this.paquetes = paquetes;
		}
		
		public void addFan() {
	        FacesMessage message = null;

	        boolean loggedIn = false;
	        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
	        HttpSession session = request.getSession();


	            Fan fan = (Fan) session.getAttribute("user");
	            fan.setPais("dsasfdd");

	            if(s.getFans() == null) {
	               List<Fan>  listFan = new ArrayList<Fan>();
	               listFan.add(fan);
	               s.setFans(listFan);
	            }else {
	                s.getFans().add(fan);
	            }

	            em.getTransaction().begin();
	            em.merge(s);
	            em.getTransaction().commit();
	            loggedIn = true;
	            message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Ya eres su fan!!!", nick);
	            FacesContext.getCurrentInstance().addMessage(null, message);
	            PrimeFaces.current().ajax().addCallbackParam("loggedIn", loggedIn);

	        }

		public List<DtPaquete> getP() {
			return p;
		}

		public void setP(List<DtPaquete> p) {
			this.p = p;
		}
		
		public void CrearPaquete() throws IOException {

		     Paquete pa = new Paquete();

		     pa.setNombre(nombrePaq);
		     pa.setPremium(prem);
		     
		     if(prem) {
		    	 pa.setPribacidad(prem);
		     }
		     else {
		     pa.setPribacidad(priv);
		     }
		     

		     List<Paquete> p = s.getPaquetes();
		     p.add(pa);
		     s.setPaquetes(p);
		     
		     em.getTransaction().begin();
		     em.merge(s);
		     em.getTransaction().commit();
		}

		/**
		 * @return the nombrePaq
		 */
		public String getNombrePaq() {
			return nombrePaq;
		}

		/**
		 * @param nombrePaq the nombrePaq to set
		 */
		public void setNombrePaq(String nombrePaq) {
			this.nombrePaq = nombrePaq;
		}

		/**
		 * @return the prem
		 */
		public Boolean getPrem() {
			return prem;
		}

		/**
		 * @param prem the prem to set
		 */
		public void setPrem(Boolean prem) {
			this.prem = prem;
		}

		/**
		 * @return the priv
		 */
		public Boolean getPriv() {
			return priv;
		}

		/**
		 * @param priv the priv to set
		 */
		public void setPriv(Boolean priv) {
			this.priv = priv;
		}
		
		public String cargarSaldo() {
	        FacesMessage message = null;
	        boolean loggedIn = false;

	        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
	        HttpSession session = request.getSession();
	        Usuario art2 = (Usuario) session.getAttribute("user");
	        Usuario us = em.find(Usuario.class, art2.getNick());
	        
	        us.setSaldo(us.getSaldo() + this.monto);
	        
	        em.getTransaction().begin();
	        em.merge(us);
	        em.getTransaction().commit();


			loggedIn = true;
	        message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Carga de saldo exitosa", nick);
	        FacesContext.getCurrentInstance().addMessage(null, message);
	        PrimeFaces.current().ajax().addCallbackParam("loggedIn", loggedIn);
	        return "sitio.xhtml?aNick="+a.getNick()+"&faces-redirect=true";
	    }
		
		
		public int PreCompra(int idContenido2,int idpaquete2,String tipo2) {
			List<Paquete> l= s.getPaquetes();
			int res = 0;
			this.monto = 0 ;
			this.monto2 = null;
			List<Integer> con2 = new ArrayList<Integer>();
			List<Integer> mon = new ArrayList<Integer>();
			for (Paquete p : l) {
				
				if(p.getIdPaquete()==idpaquete2 ) {
					List<Contenido> lc = p.getContenidos();
					if(tipo2.equals("paquete")) {
						for (Contenido c : lc) {
							if(!this.isPermiso(c.getIdContenido())) {
								res = res + c.getPrecio();
								mon.add(c.getPrecio());
								con2.add(c.getIdContenido());
							}
							
						}
						
					}
					else {
						for (Contenido c : lc) {
							if(c.getIdContenido() == idContenido2 ) {
								res = c.getPrecio();
							}
						}
					}
				}
			}
			
			this.con = con2;
			this.tipo = tipo2;
			this.idPaquete = idpaquete2;
			this.idContenido = idContenido2;
			this.monto = res;
			this.monto2 = mon;
			this.rend = true;
			return res;
		}
		
		public Boolean isValor(int idpaquete) {
			List<Paquete> l= s.getPaquetes();
			int res = 0;
			
			for (Paquete p : l) {
				if(p.getIdPaquete()==idpaquete ) {
					List<Contenido> lc = p.getContenidos();
					for (Contenido c : lc) {
						res = res + c.getPrecio();
					}
				}
			}

			if(res == 0) {
				return false;
			}
			return true;
		}
		
		public void comprar() {
	        FacesMessage message = null;
	        boolean loggedIn = false;
	        String men ;

	        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
	        HttpSession session = request.getSession();
	        Usuario us2 = (Usuario) session.getAttribute("user");
	        if(us2 == null) {
	        	return;
	        }
	        Usuario art = em.find(Usuario.class, us2.getNick());
	        int aux = 0;
	        if(art.getSaldo()<monto) {
	        	men = "Saldo insuficiente";
	        	
	        }
	        else {
	        	if(this.tipo.equals("paquete")) {
					for (int c : this.con) {
						 Comprar com = new  Comprar(this.monto2.get(aux), c, this.s.getsitioId(), art.getNick());
						 art.setSaldo(art.getSaldo()-this.monto2.get(aux));
						 double contr = this.monto2.get(aux)*(0.05);
						 Artista us = em.find(Artista.class, a.getNick());
						 us.setSaldo(a.getSaldo()+(this.monto2.get(aux)-contr));
						 us.setContri(us.getContri()+contr);
			        	 em.getTransaction().begin();
			        	 em.merge(us);
			        	 em.merge(art);
			        	 em.merge(com);
			        	 em.getTransaction().commit();
			        	 aux = aux + 1;	
					}
	        	}
	        	else {
	        		Comprar com = new  Comprar(this.monto, this.idContenido , this.s.getsitioId(), art.getNick());
		        	 art.setSaldo(art.getSaldo()-monto);
		        	 double contr = this.monto*(0.05);
					 Artista us = em.find(Artista.class, a.getNick());
					 us.setSaldo(a.getSaldo()+(this.monto2.get(aux)-contr));
					 us.setContri(us.getContri()+contr);
		        	 em.getTransaction().begin();
		        	 em.merge(us);
		        	 em.merge(art);
		        	 em.merge(com);
		        	 em.getTransaction().commit();
	        	}
	        	if(this.con.isEmpty()) {
	        	men = "no hay contenido para comprar";
	        	}
	        	else {
	        	men = "compra exitosa"; 
	        	}
	        	FacesContext context = FacesContext.getCurrentInstance();
	        	context.getExternalContext().getFlash().setKeepMessages(true);
		        context.addMessage("msg", new FacesMessage(men));
	        	try {
					FacesContext.getCurrentInstance().getExternalContext().redirect("/TodoArte/template/sitio.xhtml?aNick="+a.getNick());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	        }
	        
	       


			loggedIn = true;
	        message = new FacesMessage(FacesMessage.SEVERITY_INFO,men,nick);
	        FacesContext.getCurrentInstance().addMessage(null, message);
	        PrimeFaces.current().ajax().addCallbackParam("loggedIn", loggedIn);
	        
	    }
		
		public void comprarVip() {
	        FacesMessage message = null;
	        boolean loggedIn = false;
	        String men ;

	        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
	        HttpSession session = request.getSession();

	        

    		if (session != null && session.getAttribute("user") != null) {
    			Usuario us = (Usuario) session.getAttribute("user");
    			List<FanPremium> fa2 = s.getFansPremium();
    			Usuario art = em.find(Usuario.class, us.getNick());

    		 if (art instanceof Fan) {
    			 if(art.getSaldo()<200) {
					 men = "Saldo insuficiente";
				 }
				 
		        else {
    			 
    			for (FanPremium aux : fa2) {
					
    				if (aux.getNick().equals(us.getNick())) {
    					
    					Date d = aux.getFechaCaducidad();
    					Calendar cal = Calendar.getInstance();
    					cal.setTime(d);
    					cal.add(Calendar.DAY_OF_YEAR,+30);
    					Date r = new Date(cal.getTime().getTime());
    					aux.setFechaCaducidad(r);
    					art.setSaldo(art.getSaldo()-200);
    					
    					 double contr = 200*(0.10);
    					 Artista us2 = em.find(Artista.class, a.getNick());
    					 us2.setSaldo(a.getSaldo()+(200-contr));
    					 us2.setContri(us2.getContri()+contr);
    					
    					em.getTransaction().begin();
    					em.merge(us2);
   		        	 	em.merge(art);
   		        	 	em.merge(aux);
   		        	 	em.getTransaction().commit();
    					men = "Fan premium renovado correctamente";
    					loggedIn = true;
    			        message = new FacesMessage(FacesMessage.SEVERITY_INFO,men , nick);
    			        FacesContext.getCurrentInstance().addMessage(null, message);
    			        PrimeFaces.current().ajax().addCallbackParam("loggedIn", loggedIn);
    			        return;
    				}
				}
	        
	       

				 
		        	 
		        	 FanPremium fa = new FanPremium(art.getNick());
		        	 art.setSaldo(art.getSaldo()-200);
		        	 List<FanPremium> aux = s.getFansPremium();
		        	 aux.add(fa);
		        	 s.setFansPremium(aux);
		        	 
		        	 double contr = 200*(0.10);
					 Artista us2 = em.find(Artista.class, a.getNick());
					 us2.setSaldo(a.getSaldo()+(200-contr));
					 us2.setContri(us2.getContri()+contr);
		        	 
		        	 em.getTransaction().begin();
		        	 em.merge(us2);
		        	 em.merge(art);
		        	 em.merge(s);
		        	 em.getTransaction().commit();
		        	 men = "Ahora eres un Fan premium";
		        	
		        }
				 
				}
	        
				else {
					men = "No eres un Fan";
					}

	        
	        
	       


			loggedIn = true;
	        message = new FacesMessage(FacesMessage.SEVERITY_INFO,men , nick);
	        FacesContext.getCurrentInstance().addMessage(null, message);
	        PrimeFaces.current().ajax().addCallbackParam("loggedIn", loggedIn);
	        
    		}
	        
	    }
		
		
		public void pagarMen() {
	        FacesMessage message = null;
	        boolean loggedIn = false;
	        String men ;

	        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
	        HttpSession session = request.getSession();
	        Usuario art = (Usuario) session.getAttribute("user");
	        

    		if (session != null && session.getAttribute("user") != null) {
    			Usuario us = (Usuario) session.getAttribute("user");
    			List<mensualidad> fa2 = (List<mensualidad>) em.createNativeQuery("SELECT * FROM mensualidad", mensualidad.class).getResultList();
    			

    		 if (art instanceof Artista) {
    			 if(art.getSaldo()<500) {
					 men = "Saldo insuficiente";
				 }
				 
		        else {
    			 
    			for (mensualidad aux : fa2) {
					
    				if (aux.getNick().equals(us.getNick())) {
    					
    					Date d = aux.getFechaCaducidad();
    					Calendar cal = Calendar.getInstance();
    					cal.setTime(d);
    					cal.add(Calendar.DAY_OF_YEAR,+30);
    					Date r = new Date(cal.getTime().getTime());
    					aux.setFechaCaducidad(r);
    					art.setSaldo(art.getSaldo()-500);
    					em.getTransaction().begin();
   		        	 	em.merge(art);
   		        	 	em.merge(aux);
   		        	 	em.getTransaction().commit();
    					men = "Pago renovado correctamente";
    					loggedIn = true;
    			        message = new FacesMessage(FacesMessage.SEVERITY_INFO,men , nick);
    			        FacesContext.getCurrentInstance().addMessage(null, message);
    			        PrimeFaces.current().ajax().addCallbackParam("loggedIn", loggedIn);
    			        return;
    				}
				}
	        
	       

				 
		        	 
		        	 mensualidad fa = new mensualidad(art.getNick());
		        	 art.setSaldo(art.getSaldo()-500);
		        	 em.getTransaction().begin();
		        	 em.merge(art);
		        	 em.merge(fa);
		        	 em.getTransaction().commit();
		        	 men = "Pago realizado correctamente";
		        	
		        }
				 
				}
	        
				else {
					men = "No eres un Fan";
					}

	        
	        
	       


			loggedIn = true;
	        message = new FacesMessage(FacesMessage.SEVERITY_INFO,men , nick);
	        FacesContext.getCurrentInstance().addMessage(null, message);
	        PrimeFaces.current().ajax().addCallbackParam("loggedIn", loggedIn);
	        
    		}
	        
	    }
		


		public List<SelectItem> getTarjItem() {
			return tarjItem;
		}

		public void setTarjItem(List<SelectItem> tarjItem) {
			this.tarjItem = tarjItem;
		}

		public String getTarjFav() {
			return tarjFav;
		}

		public void setTarjFav(String tarjFav) {
			this.tarjFav = tarjFav;
		}

		public String getNumeroTarj() {
			return numeroTarj;
		}

		public void setNumeroTarj(String numeroTarj) {
			this.numeroTarj = numeroTarj;
		}

		public String getFechaTarj() {
			return fechaTarj;
		}

		public void setFechaTarj(String fechaTarj) {
			this.fechaTarj = fechaTarj;
		}

		public String getCodigoTarj() {
			return codigoTarj;
		}

		public void setCodigoTarj(String codigoTarj) {
			this.codigoTarj = codigoTarj;
		}

		public String getTelTarj() {
			return telTarj;
		}

		public void setTelTarj(String telTarj) {
			this.telTarj = telTarj;
		}

		public String getMsj2() {
			return msj2;
		}

		public void setMsj2(String msj2) {
			this.msj2 = msj2;
		}

		public String getTarjetaa() {
			return tarjetaa;
		}

		public void setTarjetaa(String tarjetaa) {
			this.tarjetaa = tarjetaa;
		}

		public int getIdPaquete() {
			return idPaquete;
		}

		public void setIdPaquete(int idPaquete) {
			this.idPaquete = idPaquete;
		}

		public Double getSaldo() {
			return saldo;
		}

		public void setSaldo(Double saldo) {
			this.saldo = saldo;
		}

		public int getMonto() {
			return monto;
		}

		public void setMonto(int monto) {
			this.monto = monto;
		}
		
		public boolean isSkip() {
            return skip;
        }

        public void setSkip(boolean skip) {
            this.skip = skip;
        }

        public String onFlowProcess(FlowEvent event) {
            if(skip) {
                skip = false;   //reset in case user goes back
                return "confirm";
            }
            else {
                return event.getNewStep();
            }
        }
        
    	public Boolean isPermiso(int idCont) {
    		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
    		HttpSession session = request.getSession();
    		if (session != null && session.getAttribute("user") != null) {
    			Usuario us = (Usuario) session.getAttribute("user");
    			List<FanPremium> fa = s.getFansPremium();
    			
    			if(this.isSecionArtistaDueño()) {
    				return true;
    				
    			}
    			
    			for (FanPremium aux : fa) {
					
    				if (aux.getNick().equals(us.getNick())) {
    				  return true;
    				}
				} 
    			 List<Comprar> resultList = (List<Comprar>) em.createNativeQuery("SELECT * FROM comprar", Comprar.class).getResultList();

    			 
    		     for (Comprar aux2 : resultList) {
    		    	 
     				if (aux2.getIdContenido() == idCont && aux2.getNick2().equals(us.getNick() )) {
     				  return true;
     				}
 				} 
    			}
    		   return false;


    		

    	};
    	
    	public Boolean isFan() {
    		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
    		HttpSession session = request.getSession();
    		if (session != null && session.getAttribute("user") != null) {
    			Usuario us = (Usuario) session.getAttribute("user");
    			List<Fan> fa = s.getFans();
    			
    			for (Fan aux : fa) {
					
    				if (aux.getNick().equals(us.getNick())) {
    				  return true;
    				}
				} 
    			
    			}
    		   return false;

    	}
    	
    	public Boolean isVip() {
    		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
    		HttpSession session = request.getSession();
    		if (session != null && session.getAttribute("user") != null) {
    			Usuario us = (Usuario) session.getAttribute("user");
    			List<FanPremium> fa = s.getFansPremium();
    			
    			
    			for (FanPremium aux : fa) {
					
    				if (aux.getNick().equals(us.getNick())) {
    				  return true;
    				}
				} 
    			
    		}
    		return false;
    	}
    	
    	public Boolean isMen() {
    		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
    		HttpSession session = request.getSession();
    		if (session != null && session.getAttribute("user") != null) {
    			Usuario us = (Usuario) session.getAttribute("user");
    			List<mensualidad> fa = (List<mensualidad>) em.createNativeQuery("SELECT * FROM mensualidad", mensualidad.class).getResultList();
    			
    			
    			for (mensualidad aux : fa) {
					
    				if (aux.getNick().equals(us.getNick())) {
    				  return true;
    				}
				} 
    			
    		}
    		return false;
    	}

		public String getFechaCaducidad() {
			return FechaCaducidad;
		}

		public void setFechaCaducidad(String fechaCaducidad) {
			FechaCaducidad = fechaCaducidad;
		}

		public int getIdContenido() {
			return idContenido;
		}

		public void setIdContenido(int idContenido) {
			this.idContenido = idContenido;
		}

		public String getTipo() {
			return tipo;
		}

		public void setTipo(String tipo) {
			this.tipo = tipo;
		}

		public List<Integer> getCon() {
			return con;
		}

		public void setCon(List<Integer> con) {
			this.con = con;
		}

		public Boolean getRend() {
			return rend;
		}

		public void setRend(Boolean rend) {
			this.rend = rend;
		};
    	
		
		
		public String getTex() {
			return tex;
		}

		public void setTex(String tex) {
			this.tex = tex;
		}

		public List<Mensaje> cargarMs(){
			return chat.cargarMs(s.getsitioId());
		}
		
		public void addMs(){
		
			chat.addMs(Integer.valueOf(id), nick, tex);
			tex = "";
			updateMS.send("updateMS");
		}
		
	    public void increment() {
	        number++;
	    }
		
		public String IniciarChat(){
			chat.IniciarChat(Integer.valueOf(id));
			FacesContext context = FacesContext.getCurrentInstance();
	        context.addMessage(null, new FacesMessage("Successful",  "Chat Online") );
	        return "sitio.xhtml?aNick="+a.getNick()+"&faces-redirect=true";
		}
		
		public String FinChat(){
			chat.FinChat(Integer.valueOf(id));
			FacesContext context = FacesContext.getCurrentInstance();
	        context.addMessage(null, new FacesMessage("Successful",  "Chat Offline") );
	        return "sitio.xhtml?aNick="+a.getNick()+"&faces-redirect=true";
		}

		public int getNumber() {
			return number;
		}

		public void setNumber(int number) {
			this.number = number;
		}
		
		public boolean isChatOn(){
			return chat.isChatOn(Integer.valueOf(id));
		}
		
	    public void prepararReport() {
	    	FacesContext fc = FacesContext.getCurrentInstance();
	        Map<String,String> params = 
	           fc.getExternalContext().getRequestParameterMap();
	       this.idrep = Integer.valueOf(params.get("id4")) ; 

	    }
		
		public void reportarContenido() {
			HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
    		HttpSession session = request.getSession();
    		if (session != null && session.getAttribute("user") != null) {
    			Usuario us = (Usuario) session.getAttribute("user");
				
	
		        Contenido content = em.find(Contenido.class, this.idrep);
		        if(content!=null) {
		            String reportelarg = "Fan :"+us.getNick()+"  Reporte : "+reportFav;
		            content.getReportes().add(reportelarg);
		            em.getTransaction().begin();
		            em.merge(content);
		            em.getTransaction().commit();
		            FacesContext context = FacesContext.getCurrentInstance();
		            context.getExternalContext().getFlash().setKeepMessages(true);
		            context.addMessage(null, new FacesMessage("Reporte Enviado"));
		        }else {
		            FacesContext context = FacesContext.getCurrentInstance();
		            context.getExternalContext().getFlash().setKeepMessages(true);
		            context.addMessage(null, new FacesMessage("No se pudo reportar, Intentelo mas tarde"));
		        }
	
	
		    }
		}

		public List<String> getReportes() {
			return reportes;
		}

		public void setReportes(List<String> reportes) {
			this.reportes = reportes;
		}

		public String getReportFav() {
			return reportFav;
		}

		public void setReportFav(String reportFav) {
			this.reportFav = reportFav;
		}
		
		public void banFan() {
			FacesContext fc = FacesContext.getCurrentInstance();
	        Map<String,String> params = 
	           fc.getExternalContext().getRequestParameterMap();
	        String nick2 = params.get("nick2") ; 
			
			
	        Sitio sit = em.find(Sitio.class,s.getsitioId());
	        Fan f = em.find(Fan.class,nick2);
	        
	        sit.getListaNegra().add(nick2);
	        sit.getFans().remove(f);
	        em.getTransaction().begin();
	        em.merge(sit);
	        em.getTransaction().commit();
	        System.out.println("Agrego");
	        FacesContext context = FacesContext.getCurrentInstance();
	        context.getExternalContext().getFlash().setKeepMessages(true);
	        context.addMessage(null, new FacesMessage("Fan Baneado"));
	    }
	    public void unbanFan() {
	    	FacesContext fc = FacesContext.getCurrentInstance();
	        Map<String,String> params = 
	           fc.getExternalContext().getRequestParameterMap();
	        String nick2 = params.get("nick3") ; 
	    	
	        Sitio sit = em.find(Sitio.class,s.getsitioId());
	        Fan f = em.find(Fan.class,nick2);
	        
	        sit.getListaNegra().remove(nick2);
	        sit.getFans().add(f);
	        em.getTransaction().begin();
	            em.merge(sit);
	        em.getTransaction().commit();
	        System.out.println("quito");

	        FacesContext context = FacesContext.getCurrentInstance();
	        context.getExternalContext().getFlash().setKeepMessages(true);
	        context.addMessage(null, new FacesMessage("Fan Desbaneado"));

	    }
	    
	    private List<Fan> fanes;
	    private List<String> listanegra;
	    
	    public List<String> listarnegra(){
	        Sitio sit = em.find(Sitio.class,s.getsitioId());

	       return this.listanegra = sit.getListaNegra();
	    }

	    public List<Fan> listarfanes(){
	        Sitio sit = em.find(Sitio.class,s.getsitioId());
	        return this.fanes = sit.getFans();
	    }

		public List<Fan> getFanes() {
			return fanes;
		}

		public void setFanes(List<Fan> fanes) {
			this.fanes = fanes;
		}

		public List<String> getListanegra() {
			return listanegra;
		}

		public void setListanegra(List<String> listanegra) {
			this.listanegra = listanegra;
		}
		
		public void consultaFan() {
	    	FacesContext fc = FacesContext.getCurrentInstance();
	        Map<String,String> params = 
	           fc.getExternalContext().getRequestParameterMap();
	        String nick2 = params.get("nick") ; 
			
            Fan us = em.find(Fan.class, nick2);
            if(us!=null) {
                this.setUs(us);
            }
		}
		
		private Fan us;


		public Fan getUs() {
			return us;
		}

		public void setUs(Fan us) {
			this.us = us;
		}
		
		public void resNotis(String usN) {
			 System.out.println(usN);
			 Usuario us = em.find(Usuario.class, usN);
			 us.setNotificacionesNuevas(0);
			 em.getTransaction().begin();
			 em.merge(us);
			 em.getTransaction().commit();
		}
		
		private String nick2;
		private String pass2;
		@EJB
		private ServiciosSeguridad servicioSeguridad;
		
	    public String login2() {
	    	FacesMessage message = null;
	        boolean loggedIn = false;
	        DtLogin data= new DtLogin(pass2,nick2);
	        Usuario user = servicioSeguridad.existeUsuario(data);
	        FacesContext context = FacesContext.getCurrentInstance();  
	        if (user == null) {
	            context.addMessage(null, new FacesMessage("Datos incorrectos, intentelo de nuevo"));
	            nick2 = null;
	            pass2 = null;
	            return null;
	        } else {
	        	if(user instanceof Artista) {
	                context.getExternalContext().getSessionMap().put("user", user); 
	                loggedIn = true;
	                message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Bienvenido", nick);
	                FacesContext.getCurrentInstance().addMessage(null, message);
	                PrimeFaces.current().ajax().addCallbackParam("loggedIn", loggedIn);
	 
	        	}else if(user instanceof Fan) {
	        		context.getExternalContext().getSessionMap().put("user", user);  
	        		if(!this.isFan()) {
	        		Fan us2 = em.find(Fan.class, nick2);
	        		s.getFans().add(us2);
	        		em.getTransaction().begin();
	        		em.merge(s);
	        		em.getTransaction().commit();
	                }
	                loggedIn = true;
	                message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Bienvenido", nick);
	                FacesContext.getCurrentInstance().addMessage(null, message);
	                PrimeFaces.current().ajax().addCallbackParam("loggedIn", loggedIn);
	        		
	        	}
	        	return "Error extraño";
	        }
	    }

		public String getNick2() {
			return nick2;
		}

		public void setNick2(String nick2) {
			this.nick2 = nick2;
		}

		public String getPass2() {
			return pass2;
		}

		public void setPass2(String pass2) {
			this.pass2 = pass2;
		}

		public String getFechaCaducidad2() {
			return FechaCaducidad2;
		}

		public void setFechaCaducidad2(String fechaCaducidad2) {
			FechaCaducidad2 = fechaCaducidad2;
		}
		
		public Boolean isSecionArtista() {
			HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
			HttpSession session = request.getSession();
			if (session != null && session.getAttribute("user") != null) {
				Usuario us = (Usuario) session.getAttribute("user");
				if (us instanceof Artista) {
					  return true;
					}
				else {
					  return false;
					}


			}
			 return false;

		};
	    
}