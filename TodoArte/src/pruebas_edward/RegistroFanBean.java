package pruebas_edward;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.primefaces.PrimeFaces;

import clases.Artista;
import clases.Fan;
import clases.Sitio;
import clases.Usuario;
import interfaces.SitioDAO;
import interfaces.UsuarioDAO;
@Named(value = "RegistroFanBean")
@RequestScoped
@TransactionManagement(TransactionManagementType.BEAN)
public class RegistroFanBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@EJB
	private UsuarioDAO usuarioDAO;
	@EJB
	private SitioDAO sitioDAO;
	private String nick;

	private String nickErr;
	
	private ArrayList<SelectItem> listaP ;	
	
	private String pais;
	private String pass;
	private String correo;
	private String sexoEleg;
	
	private List<SelectItem> listaS;
	
	private List<String> catsel;
	private List<SelectItem> catItem;
	
	private EntityManagerFactory emf = Persistence.createEntityManagerFactory("TodoArte");
	private EntityManager em = emf.createEntityManager();
	
	@PostConstruct
	public void init() {
		//lista de colores utilizando la classe SelecItem
		listaS = new ArrayList<SelectItem>();
		listaS.add(new SelectItem(1, "Hombre"));
		listaS.add(new SelectItem(2, "Mujer"));
		List<String> cat = usuarioDAO.listarCategorias();
		catItem = new ArrayList<SelectItem>();

		for(String str : cat)
		{
			catItem.add(new SelectItem(str, str)); 
		}
		listaP = new ArrayList<SelectItem>();
		listaP.add(new SelectItem("Alemania", "Alemania"));
		listaP.add(new SelectItem("Argentina", "Argentina"));
		listaP.add(new SelectItem("Armenia", "Armenia"));
		listaP.add(new SelectItem("Bolivia", "Bolivia"));
		listaP.add(new SelectItem("Brasil", "Brasil"));
		listaP.add(new SelectItem("But�n", "But�n"));
		listaP.add(new SelectItem("Cabo Verde", "Cabo Verde"));
		listaP.add(new SelectItem("Camboya", "Camboya"));
		listaP.add(new SelectItem("Camer�n", "Camer�n"));
		listaP.add(new SelectItem("Canad�", "Canad�"));
		listaP.add(new SelectItem("Chile", "Chile"));
		listaP.add(new SelectItem("China", "China"));
		listaP.add(new SelectItem("Colombia", "Colombia"));
		listaP.add(new SelectItem("Corea del Sur", "Corea del Sur"));
		listaP.add(new SelectItem("Costa Rica", "Costa Rica"));
		listaP.add(new SelectItem("Dinamarca", "Dinamarca"));
		listaP.add(new SelectItem("M�xico", "M�xico"));
		listaP.add(new SelectItem("M�naco", "M�naco"));
		listaP.add(new SelectItem("Mongolia", "Mongolia"));
		listaP.add(new SelectItem("Noruega", "Noruega"));
		listaP.add(new SelectItem("Nueva Zelanda", "Nueva Zelanda"));
		listaP.add(new SelectItem("Paraguay", "Paraguay"));
		listaP.add(new SelectItem("Per�", "Per�"));
		listaP.add(new SelectItem("Polonia", "Polonia"));
		listaP.add(new SelectItem("Portugal", "Portugal"));
		listaP.add(new SelectItem("Reino Unido", "Reino Unido"));
		listaP.add(new SelectItem("Uruguay", "Uruguay"));
		
		
	}
	public RegistroFanBean() {

	}

	public String getNickErr() {
		return nickErr;
	}
	public void setNickErr(String nickErr) {
		this.nickErr = nickErr;
	}

	public String getNick() {
		return nick;
	}
	public void setNick(String nick) {
		this.nick = nick;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	public String getSexoEleg() {
		return sexoEleg;
	}
	public void setSexoEleg(String sexoEleg) {
		this.sexoEleg = sexoEleg;
	}
	public List<SelectItem> getListaS() {
		return listaS;
	}
	public void setListaS(List<SelectItem> listaS) {
		this.listaS = listaS;
	}
	public List<SelectItem> getCatItem() {
		return catItem;
	}
	public void setCatItem(List<SelectItem> catItem) {
		this.catItem = catItem;
	}
	
	public SitioDAO getSitioDAO() {
		return sitioDAO;
	}
	public void setSitioDAO(SitioDAO sitioDAO) {
		this.sitioDAO = sitioDAO;
	}
	public UsuarioDAO getUsuarioDAO() {
		return usuarioDAO;
	}
	public void setUsuarioDAO(UsuarioDAO usuarioDAO) {
		this.usuarioDAO = usuarioDAO;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public void registroFan() {
		FacesMessage message = null;
        boolean loggedIn = false;
		
		Usuario us= em.find(Usuario.class,nick);
		if(us == null) {
		Fan fa = new Fan();
		fa.setNick(nick);
		fa.setPass(pass);
		fa.setCorreo(correo);
		fa.setPerfil(1);
		fa.setGustos(catsel);
		int foo2 = Integer.parseInt(sexoEleg);
		if(foo2==1){
			fa.setSexo("Hombre");
		}else {
			fa.setSexo("Mujer");
		}
		fa.setPais(pais);
		
		sitioDAO.registrarFan(fa);
		loggedIn = true;
		us= em.find(Usuario.class,nick);
		FacesContext context = FacesContext.getCurrentInstance();  
		context.getExternalContext().getSessionMap().put("user", us); 
		
        message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Bienvenido", nick);
        FacesContext.getCurrentInstance().addMessage(null, message);
        PrimeFaces.current().ajax().addCallbackParam("loggedIn", loggedIn);

	}
		else {
			message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Registro Error", "Nick ya esta en uso");

			loggedIn = false;
			FacesContext.getCurrentInstance().addMessage(null, message);
			PrimeFaces.current().ajax().addCallbackParam("loggedIn", loggedIn);
			
		}	
	}
	
	public void registroFan2() {
		FacesMessage message = null;
        boolean loggedIn = false;
		
		Usuario us= em.find(Usuario.class,nick);
		if(us == null) {
		Fan fa = new Fan();
		fa.setNick(nick);
		fa.setPass(pass);
		fa.setCorreo(correo);
		fa.setPerfil(1);
		fa.setGustos(catsel);
		int foo2 = Integer.parseInt(sexoEleg);
		if(foo2==1){
			fa.setSexo("Hombre");
		}else {
			fa.setSexo("Mujer");
		}
		fa.setPais(pais);
		
		sitioDAO.registrarFan(fa);
		
		String param=FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("nickAr");
		Artista a = em.find(Artista.class,param);
		Fan f = em.find(Fan.class,nick);
		Sitio s = em.find(Sitio.class,a.getSitio().getsitioId());
		s.getFans().add(f);
		em.getTransaction().begin();
		em.merge(s);
		em.getTransaction().commit();	
		
		
		loggedIn = true;
		us= em.find(Usuario.class,nick);
		FacesContext context = FacesContext.getCurrentInstance();  
		context.getExternalContext().getSessionMap().put("user", us); 
		
        message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Bienvenido", nick);
        FacesContext.getCurrentInstance().addMessage(null, message);
        PrimeFaces.current().ajax().addCallbackParam("loggedIn", loggedIn);

	}
		else {
			message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Registro Error", "Nick ya esta en uso");

			loggedIn = false;
			FacesContext.getCurrentInstance().addMessage(null, message);
			PrimeFaces.current().ajax().addCallbackParam("loggedIn", loggedIn);
			
		}	
	}
	
	
	
	
	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}
	public ArrayList<SelectItem> getListaP() {
		return listaP;
	}
	public void setListaP(ArrayList<SelectItem> listaP) {
		this.listaP = listaP;
	}
	public List<String> getCatsel() {
		return catsel;
	}
	public void setCatsel(List<String> catsel) {
		this.catsel = catsel;
	}
	
	
	

}
