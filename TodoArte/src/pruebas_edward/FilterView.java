package pruebas_edward;

import java.io.Serializable;

import java.util.List;
import java.util.Locale;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import clases.Artista;
import interfaces.UsuarioDAO;


@Named("dtFilterView123")
@SessionScoped
public class FilterView implements Serializable {
 
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
    
 
    private List<Artista> listarArtistas;
    private Artista artista;
    private List<Artista> artistasFiltrados;
	@EJB
	private UsuarioDAO usuarioDAO;
	
    @PostConstruct
    public void init() {
    	listarArtistas = usuarioDAO.listarArtistasUnban();
    }
 
 
    public boolean globalFilterFunction(Object value, Object filter, Locale locale) {
        String filterText = (filter == null) ? null : filter.toString().trim().toLowerCase();
        if (filterText == null || filterText.equals("")) {
            return true;
        }
 
        Artista artista = (Artista) value;
        return artista.getNick().toLowerCase().contains(filterText);
    }
 


	public List<Artista> getArt1() {
		return listarArtistas;
	}

	public void setArt1(List<Artista> art1) {
		this.listarArtistas = art1;
	}


	public List<Artista> getListarArtistas() {
		return listarArtistas;
	}


	public void setListarArtistas(List<Artista> listarArtistas) {
		this.listarArtistas = listarArtistas;
	}


	public Artista getArtista() {
		return artista;
	}


	public void setArtista(Artista artista) {
		this.artista = artista;
	}


	public List<Artista> getArtistasFiltrados() {
		return artistasFiltrados;
	}


	public void setArtistasFiltrados(List<Artista> artistasFiltrados) {
		this.artistasFiltrados = artistasFiltrados;
	}





 

}