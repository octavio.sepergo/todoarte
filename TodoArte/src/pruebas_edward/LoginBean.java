package pruebas_edward;


import java.io.Serializable;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import org.primefaces.PrimeFaces;

import clases.Artista;
import clases.DtLogin;
import clases.Fan;
import clases.Usuario;
import interfaces.ServiciosSeguridad;
import interfaces.UsuarioDAO;

@Named(value = "LoginBean")
@RequestScoped
public class LoginBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String nick;
	private String pass;
	@EJB
	private ServiciosSeguridad servicioSeguridad;
	@EJB
	private UsuarioDAO usuarioDAO;

	public LoginBean() {
		// TODO Auto-generated constructor stub
	}

	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public ServiciosSeguridad getServicioSeguridad() {
		return servicioSeguridad;
	}

	public void setServicioSeguridad(ServiciosSeguridad servicioSeguridad) {
		this.servicioSeguridad = servicioSeguridad;
	}

    public String login() {
    	FacesMessage message = null;
        boolean loggedIn = false;
        DtLogin data= new DtLogin(pass,nick);
        Usuario user = servicioSeguridad.existeUsuario(data);
        FacesContext context = FacesContext.getCurrentInstance();  
        if (user == null) {
            context.addMessage(null, new FacesMessage("Datos incorrectos, intentelo de nuevo"));
            nick = null;
            pass = null;
            return null;
        } else {
        	if(user instanceof Artista) {
                context.getExternalContext().getSessionMap().put("user", user); 
                loggedIn = true;
                message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Bienvenido", nick);
                FacesContext.getCurrentInstance().addMessage(null, message);
                PrimeFaces.current().ajax().addCallbackParam("loggedIn", loggedIn);
 
        	}else if(user instanceof Fan) {
        	
                context.getExternalContext().getSessionMap().put("user", user);  
                loggedIn = true;
                message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Bienvenido", nick);
                FacesContext.getCurrentInstance().addMessage(null, message);
                PrimeFaces.current().ajax().addCallbackParam("loggedIn", loggedIn);
              
        	}
        	return "Error extra�o";
        }
    }
    

    
    public String logout() {
    	FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
    	return "/principalindex.xhtml?faces-redirect=true";
    }

}