package pruebas_edward;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.context.FacesContext;
import javax.faces.push.Push;
import javax.faces.push.PushContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import clases.Artista;
import clases.Chat;
import clases.Mensaje;
import clases.Sitio;
import clases.Usuario;



public class ChatBean {
	
	
	private static ChatBean intancia = null;
	
	public  static ChatBean getIntancia() {
		 
		 if (intancia==null) {
		 
			intancia = new ChatBean();
		 }
		 return intancia;
		 }
		 
		 private ChatBean(){
			 this.chat = new ArrayList<Chat>();
		 }
	
	private List<Chat> chat;
	
	public List<Chat> getChat() {
		return chat;
	}

	public void setChat(List<Chat> chat) {
		this.chat = chat;
	}
	
	public void IniciarChat(int id){
		Chat c = new Chat(id) ;
		chat.add(c);
		return;
	}
	
	public void FinChat(int id){
		ArrayList<Chat> aux = new ArrayList<Chat>();
		for (Chat chat2 : chat) {
			if(chat2.getIdSitio() != id) {
				aux.add(chat2);
			}
		}
		this.chat = aux;
		return;
	}
	
	public void addMs(int id,String nick,String tex){

		for (Chat chat2 : chat) {
			if(chat2.getIdSitio() == id) {
				Mensaje m = new Mensaje(nick,tex);
				chat2.addMs(m);
				return;
			}
		}

	}
	
	public List<Mensaje> cargarMs(int id){
		for (Chat chat2 : chat) {
			if(chat2.getIdSitio() == id) {
				if(chat2.getMs().isEmpty()) {
					Mensaje m = new Mensaje("bot","bienvenido");
					chat2.addMs(m);
					
				}
				return chat2.getMs();
			}
		}
		
		Chat aux = new Chat(id);
		Mensaje m = new Mensaje("bot","chat incativo");
		aux.addMs(m);
		return aux.getMs();
	}
	
	public boolean isChatOn(int id){
		for (Chat chat2 : chat) {
			if(chat2.getIdSitio() == id) {
				return true;
			}
		}
		
		return false;
	}
	

}
