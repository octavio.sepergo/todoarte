package pruebas_edward;

import java.io.Serializable;
import java.util.ArrayList;

import java.util.List;

import javax.annotation.PostConstruct;

import javax.ejb.EJB;

import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.enterprise.context.RequestScoped;


import javax.faces.model.SelectItem;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import clases.Artista;
import clases.Categoria;
import clases.Fan;
import clases.Sitio;

import interfaces.UsuarioDAO;


@Named(value = "ArtistaBean")
@RequestScoped

@TransactionManagement(TransactionManagementType.BEAN)
public class ArtistaBean implements Serializable{
	@EJB
	private UsuarioDAO usuarioDAO;
	private List<Artista> artistas;
	private List<Artista> artistasCat;
	private List<SelectItem> catItem;
	List<Categoria> cate;
	List<Categoria> cate2;
	List<Categoria> cate3;
	private String catFav;
	private String nick= "";
	private String nick2 = "nick..";
	private String correo = "correo..";
	private String direccion = "direccion..";
	private String nombre = "nombre..";
	private String pass = "pass..";
	private String msj = "Resultado de Busqueda...";
	private String msj2 = "Bienvenido..";
	private String msj3 = "Resultado de Busqueda...";
	private int id = 6;
	private EntityManagerFactory emf = Persistence.createEntityManagerFactory("TodoArte");
	private EntityManager em = emf.createEntityManager();
	private static final long serialVersionUID = 1L;

	public ArtistaBean() {
		
	}
	
	
	public List<Categoria> getCate() {
		return cate;
	}
	public void setCate(List<Categoria> cate) {
		this.cate = cate;
	}
	public List<Categoria> getCate2() {
		return cate2;
	}
	public void setCate2(List<Categoria> cate2) {
		this.cate2 = cate2;
	}
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@PostConstruct
	public void init() {
		/*
		Categoria c1 = new Categoria(2,"Musica",true);
		em.getTransaction().begin();
        em.persist(c1);
		em.getTransaction().commit();
		em.clear();
		Categoria c2 = new Categoria(3,"Comic",true);
		em.getTransaction().begin();
        em.persist(c2);
		em.getTransaction().commit();
		em.clear();
		Categoria c3 = new Categoria(4,"Futbol",true);
		em.getTransaction().begin();
        em.persist(c3);
		em.getTransaction().commit();
		em.clear();
		Categoria c4 = new Categoria(1,"Todo",true);
		em.getTransaction().begin();
        em.persist(c4);
		em.getTransaction().commit();
		em.clear();
		*/
		
		/*
		cate3.add(c1);
		cate3.add(c3);
		cate3.add(c4);
		cate4.add(c2);
		cate4.add(c4);
		cate5.add(c4);
		
		
		this.setCate(cate3);
		this.setCate2(cate4);
		this.setCate3(cate5);
		List<String> cat = usuarioDAO.listarCategorias();
		catItem = new ArrayList<SelectItem>();
		int cont = 0;
		for(String str : cat)
		{
			cont++;
			catItem.add(new SelectItem(cont, str)); 
		}

		
		Artista a = new Artista();
		Artista a1 = new Artista();
		Artista a2 = new Artista();
		Artista a3 = new Artista();
		Sitio s  = new Sitio();
		Sitio s1  = new Sitio();
		Sitio s2  = new Sitio();
		Sitio s3  = new Sitio();
		Fan f1 = new Fan();
		Fan f2 = new Fan();
		Fan f3 = new Fan();
		Fan f4 = new Fan();
		///////////////////////////////
		s.setTitulo("Sitio para Todos");
		s.setCategorias(this.cate);
		s.setDescripcion("descripcion");
		s.setFans(null);
		s.setFansPremium(null);
		s.setListaNegra(null);
		s.setContenidos(null);
		s.setPaquetes(null);
		s.setQA_activo(false);
		s.setsitioId(1);
		em.getTransaction().begin();
		em.persist(s);
		em.getTransaction().commit();
		em.clear();
		///////////////////////////////
		s1.setTitulo("Los Mejores Contenidos");
		s1.setCategorias(this.cate);
		s1.setDescripcion("descripcion 1");
		s1.setFans(null);
		s1.setFansPremium(null);
		s1.setListaNegra(null);
		s1.setContenidos(null);
		s1.setPaquetes(null);
		s1.setQA_activo(false);
		s1.setsitioId(2);
		em.getTransaction().begin();
		em.persist(s1);
		em.getTransaction().commit();
		em.clear();
		///////////////////////////////
		s2.setTitulo("Futbol Uruguayo");
		s2.setCategorias(this.cate2);
		s2.setDescripcion("descripcion 2");
		s2.setFans(null);
		s2.setFansPremium(null);
		s2.setListaNegra(null);
		s2.setContenidos(null);
		s2.setPaquetes(null);
		s2.setQA_activo(false);
		s2.setsitioId(3);
		em.getTransaction().begin();
		em.persist(s2);
		em.getTransaction().commit();
		em.clear();
		///////////////////////////////
		s3.setTitulo("El Mejor Sitio");
		s3.setCategorias(this.cate2);
		s3.setDescripcion("descripcion 3");
		s3.setFans(null);
		s3.setFansPremium(null);
		s3.setListaNegra(null);
		s3.setContenidos(null);
		s3.setPaquetes(null);
		s3.setQA_activo(false);
		s3.setsitioId(4);
		em.getTransaction().begin();
		em.persist(s3);
		em.getTransaction().commit();
		em.clear();
		///////////////////////////////
		a.setNick("Jorgito");
		a.setPass("123");
		a.setSexo("H");
		a.setCorreo("jorg199@hotmail.com");
		a.setBan(false);
		a.setDireccion("hola");
		a.setFechaNacimiento(null);
		a.setNombre("Jorgge Lenme");
		a.setPerfil(1);
		a.setSaldo(0);
		a.setSitio(s);
		em.getTransaction().begin();
        em.persist(a);
		em.getTransaction().commit();
		em.clear();
		///////////////////////////////
		a1.setNick("Jorgita");
		a1.setPass("123");
		a1.setSexo("M");
		a1.setCorreo("jorg@hotmail.com");
		a1.setBan(false);
		a1.setDireccion("hola");
		a1.setFechaNacimiento(null);
		a1.setNombre("Jorggita Mendez");
		a1.setPerfil(1);
		a1.setSaldo(0);
		a1.setSitio(s1);
		em.getTransaction().begin();
        em.persist(a1);
		em.getTransaction().commit();
		em.clear();
		///////////////////////////////
		a2.setNick("Joselo");
		a2.setPass("123");
		a2.setSexo("H");
		a2.setCorreo("jos@hotmail.com");
		a2.setBan(true);
		a2.setDireccion("hola");
		a2.setFechaNacimiento(null);
		a2.setNombre("Jose Martineti");
		a2.setPerfil(1);
		a2.setSaldo(0);
		a2.setSitio(s2);
		em.getTransaction().begin();
        em.persist(a2);
		em.getTransaction().commit();
		em.clear();
		///////////////////////////////
		a3.setNick("Josela");
		a3.setPass("123");
		a3.setSexo("H");
		a3.setCorreo("dasd@asdsa");
		a3.setBan(false);
		a3.setDireccion("hola");
		a3.setFechaNacimiento(null);
		a3.setNombre("Josela Perez");
		a3.setPerfil(1);
		a3.setSaldo(0);
		a3.setSitio(s3);
		em.getTransaction().begin();
        em.persist(a3);
		em.getTransaction().commit();
		em.clear();
		///////////////////////////////
		f1.setNick("Pepito");
		f1.setPass("123");
		f1.setSexo("H");
		f1.setCorreo("dasd@asdsa");
		f1.setPerfil(1);
		em.getTransaction().begin();
        em.persist(f1);
		em.getTransaction().commit();
		em.clear();
		///////////////////////////////
		f2.setNick("Carlit");
		f2.setPass("123");
		f2.setSexo("H");
		f2.setCorreo("dasd@asdsa");
		f2.setPerfil(1);
		em.getTransaction().begin();
        em.persist(f2);
		em.getTransaction().commit();
		em.clear();
		///////////////////////////////
		f3.setNick("Mario");
		f3.setPass("123");
		f3.setSexo("H");
		f3.setCorreo("dasd@asdsa");
		f3.setPerfil(1);
		em.getTransaction().begin();
        em.persist(f3);
		em.getTransaction().commit();
		em.clear();
		///////////////////////////////
		f4.setNick("Lucia");
		f4.setPass("123");
		f4.setSexo("M");
		f4.setCorreo("dasd@asdsa");
		f4.setPerfil(1);
		em.getTransaction().begin();
        em.persist(f4);
		em.getTransaction().commit();
		em.clear();
		*/
		List<String> cat = usuarioDAO.listarCategorias();
        catItem = new ArrayList<SelectItem>();
        int cont = 0;
        for(String str : cat)
        {
            cont++;
            catItem.add(new SelectItem(cont, str)); 
        }
        
		this.artistas = usuarioDAO.listarArtistasUnban();
		this.catFav = "1";
		int foo = Integer.parseInt(this.catFav);
		this.artistasCat = usuarioDAO.BuscarArtistaCategoria(foo);
		
		
	}
		
	
	public List<Artista> getArtistasCat() {
		return artistasCat;
	}
	public void setArtistasCat(List<Artista> artistasCat) {
		this.artistasCat = artistasCat;
	}
	public List<SelectItem> getCatItem() {
		return catItem;
	}
	public void setCatItem(List<SelectItem> catItem) {
		this.catItem = catItem;
	}
	public String getCatFav() {
		return catFav;
	}
	public void setCatFav(String catFav) {
		this.catFav = catFav;
	}
	public List<Artista> getArtistas() {
		return artistas;
	}

	public void setArtistas(List<Artista> artistas) {
		this.artistas = artistas;
	}

	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}

	public UsuarioDAO getUsuarioDAO() {
		return usuarioDAO;
	}

	public void setUsuarioDAO(UsuarioDAO usuarioDAO) {
		this.usuarioDAO = usuarioDAO;
	}
	
	

	public List<Categoria> getCate3() {
		return cate3;
	}

	public void setCate3(List<Categoria> cate3) {
		this.cate3 = cate3;
	}

	public String getMsj() {
		return msj;
	}

	public void setMsj(String msj) {
		this.msj = msj;
	}
	
	
	public String getNick2() {
		return nick2;
	}

	public void setNick2(String nick2) {
		this.nick2 = nick2;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public String getMsj2() {
		return msj2;
	}

	public void setMsj2(String msj2) {
		this.msj2 = msj2;
	}
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMsj3() {
		return msj3;
	}
	public void setMsj3(String msj3) {
		this.msj3 = msj3;
	}
	
	
/*@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void cargardatos() throws Exception {
		Artista a = new Artista();
		Artista a1 = new Artista();
		Artista a2 = new Artista();
		Artista a3 = new Artista();
		Sitio s  = new Sitio();
		Sitio s1  = new Sitio();
		Sitio s2  = new Sitio();
		Sitio s3  = new Sitio();
		///////////////////////////////
		s.setTitulo("titulo sitio");
		s.setCategorias(this.cate);
		s.setDescripcion("descripcion");
		s.setFans(null);
		s.setFansPremium(null);
		s.setListaNegra(null);
		s.setContenidos(null);
		s.setPaquetes(null);
		s.setQA_activo(false);
		s.setsitioId(1);
		em.getTransaction().begin();
		em.persist(s);
		em.getTransaction().commit();
		em.clear();
		///////////////////////////////
		s1.setTitulo("titulo sitio 1");
		s1.setCategorias(this.cate);
		s1.setDescripcion("descripcion 1");
		s1.setFans(null);
		s1.setFansPremium(null);
		s1.setListaNegra(null);
		s1.setContenidos(null);
		s1.setPaquetes(null);
		s1.setQA_activo(false);
		s1.setsitioId(2);
		em.getTransaction().begin();
		em.persist(s1);
		em.getTransaction().commit();
		em.clear();
		///////////////////////////////
		s2.setTitulo("titulo sitio 2");
		s2.setCategorias(this.cate2);
		s2.setDescripcion("descripcion 2");
		s2.setFans(null);
		s2.setFansPremium(null);
		s2.setListaNegra(null);
		s2.setContenidos(null);
		s2.setPaquetes(null);
		s2.setQA_activo(false);
		s2.setsitioId(3);
		em.getTransaction().begin();
		em.persist(s2);
		em.getTransaction().commit();
		em.clear();
		///////////////////////////////
		s3.setTitulo("titulo sitio 3");
		s3.setCategorias(this.cate2);
		s3.setDescripcion("descripcion 3");
		s3.setFans(null);
		s3.setFansPremium(null);
		s3.setListaNegra(null);
		s3.setContenidos(null);
		s3.setPaquetes(null);
		s3.setQA_activo(false);
		s3.setsitioId(4);
		em.getTransaction().begin();
		em.persist(s3);
		em.getTransaction().commit();
		em.clear();
		///////////////////////////////
		a.setNick("jorgito");
		a.setPass("123");
		a.setSexo("H");
		a.setCorreo("dasd@asdsa");
		a.setBan(false);
		a.setDireccion("hola");
		a.setFechaNacimiento(null);
		a.setNombre("jorgge");
		a.setPerfil(1);
		a.setSaldo(0);
		a.setSitio(s);
		em.getTransaction().begin();
        em.persist(a);
		em.getTransaction().commit();
		em.clear();
		///////////////////////////////
		a1.setNick("jorgita");
		a1.setPass("123");
		a1.setSexo("M");
		a1.setCorreo("dasd@asdsads");
		a1.setBan(false);
		a1.setDireccion("hola");
		a1.setFechaNacimiento(null);
		a1.setNombre("jorggita");
		a1.setPerfil(1);
		a1.setSaldo(0);
		a1.setSitio(s1);
		em.getTransaction().begin();
        em.persist(a1);
		em.getTransaction().commit();
		em.clear();
		///////////////////////////////
		a2.setNick("Jose");
		a2.setPass("123");
		a2.setSexo("H");
		a2.setCorreo("dasd@asdsa");
		a2.setBan(true);
		a2.setDireccion("hola");
		a2.setFechaNacimiento(null);
		a2.setNombre("jose");
		a2.setPerfil(1);
		a2.setSaldo(0);
		a2.setSitio(s2);
		em.getTransaction().begin();
        em.persist(a2);
		em.getTransaction().commit();
		em.clear();
		///////////////////////////////
		a3.setNick("josela");
		a3.setPass("123");
		a3.setSexo("H");
		a3.setCorreo("dasd@asdsa");
		a3.setBan(false);
		a3.setDireccion("hola");
		a3.setFechaNacimiento(null);
		a3.setNombre("josela");
		a3.setPerfil(1);
		a3.setSaldo(0);
		a3.setSitio(s3);
		em.getTransaction().begin();
        em.persist(a3);
		em.getTransaction().commit();
		em.clear();
	}
	*/

	public void list() {
		this.artistas = usuarioDAO.listarArtistasUnban();

	}
	
	public void buscar() {
		Artista n =usuarioDAO.buscarArtista(this.nick);
        if(n!=null) {
        	List<Artista> cat2 = new ArrayList<Artista>();
        	cat2.add(n);
	    	this.artistasCat = cat2;
	    	
	        System.out.println("Encontrado");
        }else {
	        System.out.println("Artista No encontrado");
	        
	        this.setMsj("No se han encontrado Resultado que coincida");
	        
	        this.artistasCat = usuarioDAO.BuscarArtistaRelacionBusqueda(this.nick);
        }
	  
	}
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void buscarArtCat() throws Exception{
		 System.out.println(this.catFav);
		int foo = Integer.parseInt(this.catFav);
		this.artistasCat = usuarioDAO.BuscarArtistaCategoria(foo);  

	}

}
