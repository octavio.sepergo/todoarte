package beans;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.primefaces.PrimeFaces;

import com.sun.javafx.binding.SelectBinding.AsDouble;

import clases.Artista;
import clases.Usuario;
import interfaces.UsuarioDAO;

@Path("/notis")
@Consumes({ "application/json" })
@Produces({ "application/json" })
@Named("BeanInicio")
@RequestScoped
public class BeanInicio implements Serializable{
	
	@EJB
	private UsuarioDAO usuarioDAO;
	private EntityManagerFactory emf = Persistence.createEntityManagerFactory("TodoArte");
	private EntityManager em = emf.createEntityManager();
	
	private static final long serialVersionUID = 413L;
	private Date fecha;
	private String nick;
	List<String> notis;
	
	@Path("/notificacionesNuevas")
	@POST
	public int notificacionesNuevas(String Nick) {
		
			 Usuario us2= em.find(Usuario.class,Nick);
			 return us2.getNotificacionesNuevas();

	}
	
	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}

	public BeanInicio() {
		
	}
	
	
	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String nickSecion() {
		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		HttpSession session = request.getSession();
		if (session != null && session.getAttribute("user") != null) {
			Usuario us = (Usuario) session.getAttribute("user");
			return us.getNick();
		}; 
		return "NO LOGEADO";
	};
	
	public List<String> notis() {
		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		HttpSession session = request.getSession();
		if (session != null && session.getAttribute("user") != null) {
			Usuario us = (Usuario) session.getAttribute("user");
			String n = us.getNick();
			Usuario us2 = em.find(Usuario.class, n);
			us2.setNotificacionesNuevas(0);
			//percistir us2
			return us2.getNotificaciones();
		}; 
		return null;
	}

	public List<String> getNotis() {
		return notis;
	}

	public void setNotis(List<String> notis) {
		this.notis = notis;
	};
	
	public void programarQyA() {

		FacesMessage message = null;
        boolean loggedIn = true;
		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		HttpSession session = request.getSession();
		Usuario us = (Usuario) session.getAttribute("user");
		usuarioDAO.programarQyA(us.getNick(),fecha);
        message = new FacesMessage(FacesMessage.SEVERITY_INFO, "QyA","programada");
        FacesContext.getCurrentInstance().addMessage(null, message);
        PrimeFaces.current().ajax().addCallbackParam("loggedIn", loggedIn);
	};
	
	public void alludaaa() {
	    
	        Map<String,Object> options = new HashMap<String, Object>();
	        options.put("modal", true);
	        options.put("width", 640);
	        options.put("height", 340);
	        options.put("contentWidth", "100%");
	        options.put("contentHeight", "100%");
	        options.put("headerElement", "customheader");
	         
	        PrimeFaces.current().dialog().openDynamic("../asd.xhtml", options, null);

	};
	
}
