package clases;

public class Mensaje {
	
	private String nick;
	private String texto;
	
	public Mensaje() {

	}
	
	public Mensaje(String nick, String texto) {
		super();
		this.nick = nick;
		this.texto = texto;
	}
	public String getNick() {
		return nick;
	}
	public void setNick(String nick) {
		this.nick = nick;
	}
	public String getTexto() {
		return texto;
	}
	public void setTexto(String texto) {
		this.texto = texto;
	}
	
	
	
}
