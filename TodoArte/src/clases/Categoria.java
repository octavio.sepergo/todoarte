package clases;

import java.io.Serializable;
import java.lang.String;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Categoria
 *
 */
@Entity

public class Categoria implements Serializable {

	   
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int categoriaId;
	private String nombre;
	private boolean principal;
	private static final long serialVersionUID = 1L;

	public Categoria() {
		super();
	}   
	
	public Categoria(int categoriaId, String nombre, boolean principal) {
		super();
		this.categoriaId = categoriaId;
		this.nombre = nombre;
		this.principal = principal;
	}
	
	public Categoria(String nombre, boolean principal) {
        super();
        this.nombre = nombre;
        this.principal = principal;
    }

	public int getCategoriaId() {
		return this.categoriaId;
	}

	public void setCategoriaId(int categoriaId) {
		this.categoriaId = categoriaId;
	}   
	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}   
	public boolean getPrincipal() {
		return this.principal;
	}

	public void setPrincipal(boolean principal) {
		this.principal = principal;
	}
   
}
