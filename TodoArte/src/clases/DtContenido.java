package clases;

public class DtContenido {
	int idContenido;
	String nombre;
	
	
	
	public DtContenido(int idContenido, String nombre) {
		super();
		this.idContenido = idContenido;
		this.nombre = nombre;
	}

	public int getIdContenido() {
		return idContenido;
	}
	
	public String getIdContenidoS() {
		return String.valueOf(idContenido);
	}

	public String getNombre() {
		return nombre;
	}

	
	
}
