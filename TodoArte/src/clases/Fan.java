package clases;

import clases.Usuario;
import java.io.Serializable;
import java.lang.String;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;



/**
 * Entity implementation class for Entity: Fan
 *
 */
@Entity
@DiscriminatorValue("F")
public class Fan extends Usuario implements Serializable {

	
	private String pais;
	 
	@ElementCollection
	private List<String> gustos;
	
	private static final long serialVersionUID = 1L;
 
	public Fan() {
		super();
	}   
	

	public String getPais() {
		return this.pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}   
	public List<String> getGustos() {
		return this.gustos;
	}

	public void setGustos(List<String> gustos) {
		this.gustos = gustos;
	}
   
}
