package clases;

import java.io.Serializable;
import java.lang.Integer;
import java.sql.Date;
import java.util.Calendar;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: FanPremium
 *
 */
@Entity

public class FanPremium implements Serializable {

	   
	@Id
	@GeneratedValue( strategy=GenerationType.AUTO )
	private Integer fanPremiunId;
	
	private Date fechaInicio;
	
	private Date fechaCaducidad;
	private String nick;
	
	private static final long serialVersionUID = 1L;

	public FanPremium() {
		super();
	}   
	
	public FanPremium(String nick) {
		super();
		this.nick = nick;
		fechaInicio = new Date(Calendar.getInstance().getTime().getTime());
		
		  Calendar calendar = Calendar.getInstance();
	      calendar.setTime(Calendar.getInstance().getTime()); 
	      calendar.add(Calendar.DAY_OF_YEAR,+30);  
		
		fechaCaducidad = new Date(calendar.getTime().getTime());
		
	}
	
	public Integer getFanPremiunId() {
		return this.fanPremiunId;
	}

	public void setFanPremiunId(Integer fanPremiunId) {
		this.fanPremiunId = fanPremiunId;
	}   
	public Date getFechaInicio() {
		return this.fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}   
	public Date getFechaCaducidad() {
		return this.fechaCaducidad;
	}

	public void setFechaCaducidad(Date fechaCaducidad) {
		this.fechaCaducidad = fechaCaducidad;
	}

	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}
	
	
   
}
