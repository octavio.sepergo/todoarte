package clases;

import java.io.Serializable;
import java.lang.Integer;
import java.lang.String;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Contenido
 *
 */
@Entity

public class Contenido implements Serializable {

  
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int idContenido;
	
	private String titulo;
	
	private int precio;
	
	@ElementCollection(fetch = FetchType.EAGER)
	private List<String> reportes = new ArrayList<>();
	
	private boolean ban; 
	
	private String mongo;
	
	@OneToMany
	@JoinColumn ( name = "contenidoId" )
	private List<Comentario> comentarios = new ArrayList<Comentario>();  
	
	@OneToMany
    @JoinColumn ( name = "contenidoId" )
    private List<Puntaje> Puntajes = new ArrayList<Puntaje>();
	
	private static final long serialVersionUID = 1L;

	public Contenido() {
		super();
	}   
	

	
	public Contenido(String titulo,String mongo,int precio) {
		super();
		this.titulo = titulo;
		this.mongo = mongo;
		this.precio = precio;
	}
	

	public String getTitulo() {
		return this.titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}   
  
 
	public int getIdContenido() {
		return this.idContenido;
	}

	public void setIdContenido(int idContenido) {
		this.idContenido = idContenido;
	}

	public String getMongo() {
		return mongo;
	}

	public void setMongo(String mongo) {
		this.mongo = mongo;
	}

	public List<Comentario> getComentarios() {
		return comentarios;
	}

	public void setComentarios(List<Comentario> comentarios) {
		this.comentarios = comentarios;
	}



	public int getPrecio() {
		return precio;
	}



	public void setPrecio(int precio) {
		this.precio = precio;
	}



	public List<Puntaje> getPuntajes() {
		return Puntajes;
	}



	public void setPuntajes(List<Puntaje> puntajes) {
		Puntajes = puntajes;
	}



	public List<String> getReportes() {
		return reportes;
	}



	public void setReportes(List<String> reportes) {
		this.reportes = reportes;
	}



	public boolean isBan() {
		return ban;
	}



	public void setBan(boolean ban) {
		this.ban = ban;
	}
   
		
	
}
