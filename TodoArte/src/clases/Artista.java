package clases;

import clases.Sitio;
import clases.Usuario;
import java.io.Serializable;
import java.lang.String;
import java.sql.Date;
import javax.persistence.*;

@Entity
@DiscriminatorValue("A")
public class Artista extends Usuario implements Serializable {

	
	private String nombre;
	private String direccion;
	private Date fechaNacimiento;
	private Sitio sitio;
	private boolean ban;
	private double contri;
	private static final long serialVersionUID = 1L;

	public Artista() {
		super(); 
	}   
	


	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}   
	public String getDireccion() {
		return this.direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}   
	public Date getFechaNacimiento() {
		return this.fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}   
	public Sitio getSitio() {
		return this.sitio;
	}

	public void setSitio(Sitio sitio) {
		this.sitio = sitio;
	}

	public boolean isBan() {
		return ban;
	}
	public void setBan(boolean ban) {
		this.ban = ban;
	}



	public double getContri() {
		return contri;
	}



	public void setContri(double contri) {
		this.contri = contri;
	}
	
	
}
