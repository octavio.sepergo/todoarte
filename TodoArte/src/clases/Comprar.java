package clases;

import java.io.Serializable;
import java.lang.String;
import java.sql.Date;
import java.util.Calendar;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Comprar
 *
 */
@Entity

public class Comprar implements Serializable {

	   
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int idComprar;
	private int monto;
	private int idContenido;
	private int idSitio;
	private String nick2; 
	private Date fecha;
	private static final long serialVersionUID = 1L;

	public Comprar() { 
		super();
	}   
	
	public Comprar(int monto,int idContenido,int idSitio,String nick) {
		super();
		this.monto = monto;
		this.idContenido = idContenido;
		this.idSitio = idSitio;
		fecha = new Date(Calendar.getInstance().getTime().getTime());
		this.nick2 = nick;
	}   
	
	public int getIdComprar() {
		return this.idComprar;
	}

	public void setIdComprar(int idComprar) {
		this.idComprar = idComprar;
	}   
	public int getMonto() {
		return this.monto;
	}

	public void setMonto(int monto) {
		this.monto = monto;
	}   

	public Date getFecha() {
		return this.fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public int getIdContenido() {
		return idContenido;
	}
	public void setIdContenido(int idContenido) {
		this.idContenido = idContenido;
	}
	public int getIdSitio() {
		return idSitio;
	}
	public void setIdSitio(int idSitio) {
		this.idSitio = idSitio;
	}

	public String getNick2() {
		return nick2;
	}

	public void setNick2(String nick) {
		this.nick2 = nick;
	}
   
	
}
