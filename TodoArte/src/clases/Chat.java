package clases;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.push.Push;
import javax.faces.push.PushContext;

public class Chat {
	
	private int idSitio;
	private List<Mensaje> ms;
	
	
	public Chat() {
		super();
		this.ms = new ArrayList<Mensaje>();	
	}
	
	public Chat(int idSitio) {
		super();
		this.idSitio = idSitio;
		this.ms = new ArrayList<Mensaje>();	
	}
	

	public int getIdSitio() {
		return idSitio;
	}



	public void setIdSitio(int idSitio) {
		this.idSitio = idSitio;
	}



	public List<Mensaje> getMs() {
		return ms;
	}

	public void setMs(List<Mensaje> chat) {
		this.ms = chat;
	}
	
	public void addMs(Mensaje m) {
		this.ms.add(m);
	}
	
}
