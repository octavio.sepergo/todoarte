package clases;

import java.io.Serializable;
import java.lang.String;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Usuario
 *
 */
@Entity
@Table(name="USUARIOS")
@Inheritance( strategy = InheritanceType.SINGLE_TABLE )
@DiscriminatorColumn(name="TYPE")
public class Usuario implements Serializable {

	   
	@Id
	private String nick;
	private String pass;
	private String correo;
	private String sexo;
	private double saldo;
	private int Perfil;
	@ElementCollection
	private List<String> notificaciones;
	private int notificacionesNuevas;
	private int notificacionesCargadas;
	private static final long serialVersionUID = 1L;
	

	public Usuario() {
		super();
	}   
	public String getNick() {
		return this.nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}   
	public String getPass() {
		return this.pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}   
	public String getCorreo() {
		return this.correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}   
	public int getPerfil() {
		return this.Perfil;
	}

	public void setPerfil(int Perfil) {
		this.Perfil = Perfil;
	}
	public String getSexo() {
		return this.sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	public List<String> getNotificaciones() {
		return notificaciones;
	}
	public void setNotificaciones(List<String> notificaciones) {
		this.notificaciones = notificaciones;
	}
	public int getNotificacionesNuevas() {
		return notificacionesNuevas;
	}
	public void setNotificacionesNuevas(int notificacionesNuevas) {
		this.notificacionesNuevas = notificacionesNuevas;
	}
	public int getNotificacionesCargadas() {
		return notificacionesCargadas;
	}
	public void setNotificacionesCargadas(int notificacionesCargadas) {
		this.notificacionesCargadas = notificacionesCargadas;
	}
	public double getSaldo() {
		return saldo;
	}
	public void setSaldo(double saldo) {
		this.saldo = saldo;
	} 
	
	
}
