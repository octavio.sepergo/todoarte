package clases;

import java.io.Serializable;
import java.sql.Date;
import javax.persistence.*;

@Entity

public class Calificacion implements Serializable {

	   
	@Id
	private int idCalificacion;
	private int puntaje;
	private Date fecha;
	private static final long serialVersionUID = 1L;

	public Calificacion() {
		super();
	}   
	public int getIdCalificacion() {
		return this.idCalificacion;
	}

	public void setIdCalificacion(int idCalificacion) {
		this.idCalificacion = idCalificacion;
	}   
	public int getPuntaje() {
		return this.puntaje;
	}

	public void setPuntaje(int puntaje) {
		this.puntaje = puntaje;
	}   
	public Date getFecha() {
		return this.fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
   
}
