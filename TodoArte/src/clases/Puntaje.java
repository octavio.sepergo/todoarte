package clases;



import java.io.Serializable;

import javax.persistence.Entity;


import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

 
@Entity
public class Puntaje implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id@GeneratedValue( strategy=GenerationType.AUTO )
    private Integer puntajeId;
	
	private String nick;
	
	private int puntaje;
	
	
	
	
	public Puntaje() {
		
	}
	public Puntaje(String nick,int puntaje){
		this.nick = nick;
		this.puntaje = puntaje;
	}
	
	public int getPuntaje() {
		return puntaje;
	}
	public void setPuntaje(int puntaje) {
		this.puntaje = puntaje;
	}
	public String getNick() {
		return nick;
	}
	public void setNick(String nick) {
		this.nick = nick;
	}
	public Integer getPuntajeId() {
		return puntajeId;
	}
	public void setPuntajeId(Integer puntajeId) {
		this.puntajeId = puntajeId;
	}

	
	
	
	

}
