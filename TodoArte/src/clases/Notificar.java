package clases;

import java.io.Serializable;
import java.lang.String;
import java.sql.Date;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Notificar
 *
 */
@Entity

public class Notificar implements Serializable {

	   
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int idNotificar;
	private String descripcion;
	private Date fecha;
	private static final long serialVersionUID = 1L;

	public Notificar() {
		super();
	}   
	public int getIdNotificar() {
		return this.idNotificar;
	}

	public void setIdNotificar(int idNotificar) {
		this.idNotificar = idNotificar;
	}   
	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}   
	public Date getFecha() {
		return this.fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
   
}
