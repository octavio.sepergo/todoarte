package clases;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Admin
 *
 */
@Entity 
 
public class Admin implements Serializable {

	@Id
	private String user;
	private String pass;
	private static final long serialVersionUID = 1L;

	public Admin() {
		super();
	}

	
	public Admin(String user, String pass) {
		super();
		this.user = user;
		this.pass = pass;
	}


	public String getNick() {
		return user;
	}

	public void setNick(String user) {
		this.user = user;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}
   
}
