package pruebas_admin;

import java.io.IOException;
import java.io.Serializable;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.primefaces.PrimeFaces;

import clases.Admin;
import clases.Artista;
import clases.Categoria;
import clases.Contenido;
import clases.Paquete;
import clases.Usuario;
import clases.mensualidad;
import interfaces.UsuarioDAO;

@Named(value= "AdminBean")
@ViewScoped
@TransactionManagement(TransactionManagementType.BEAN)
public class AdminBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private EntityManagerFactory emf = Persistence.createEntityManagerFactory("TodoArte");
	private EntityManager em = emf.createEntityManager();
	private String cate;
	private List<SelectItem> prinItem;
	private String prin;
	private String notiMensaj;
	private String artiNoti;
	private String FechaCaducidad; 
	
	@EJB
	private UsuarioDAO usuarioDAO;

	public AdminBean() {
		// TODO Auto-generated constructor stub
	}
	@PostConstruct
	public void init() {
		List<String>  principales = new ArrayList<String>();
		 principales.add("Principal");
		 principales.add("Secundaria");
		 prinItem = new ArrayList<SelectItem>();
		int cont = 0;
		for(String str : principales)
		{
			cont++;
			prinItem.add(new SelectItem(cont, str)); 
		}
		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        HttpSession session = request.getSession();
        Admin art2 = (Admin) session.getAttribute("admin");

        if(art2 == null ) {
        	try {
				FacesContext.getCurrentInstance().getExternalContext().redirect("loginAdmin.xhtml");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
	}
	
	public EntityManagerFactory getEmf() {
		return emf;
	}
	public void setEmf(EntityManagerFactory emf) {
		this.emf = emf;
	}
	public EntityManager getEm() {
		return em;
	}
	public void setEm(EntityManager em) {
		this.em = em;
	}
	public String getCate() {
		return cate;
	}
	public void setCate(String cate) {
		this.cate = cate;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	public List<SelectItem> getPrinItem() {
		return prinItem;
	}

	public void setPrinItem(List<SelectItem> prinItem) {
		this.prinItem = prinItem;
	}

	public String getPrin() {
		return prin;
	}

	public void setPrin(String prin) {
		this.prin = prin;
	}

	
	public String getNotiMensaj() {
		return notiMensaj;
	}
	public void setNotiMensaj(String notiMensaj) {
		this.notiMensaj = notiMensaj;
	}
	public String getArtiNoti() {
		return artiNoti;
	}
	public void setArtiNoti(String artiNoti) {
		this.artiNoti = artiNoti;
	}
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void banContenido(int id) throws IOException {
        boolean loggedIn = true;
		FacesContext context = FacesContext.getCurrentInstance();  


			Contenido cn = em.find(Contenido.class, id);
			if(cn!=null) {
				cn.setBan(true);
				em.getTransaction().begin();
				em.merge(cn);
				em.getTransaction().commit();
				em.clear();
		        context.addMessage(null, new FacesMessage("Contenido Bloqueado"));
		        PrimeFaces.current().ajax().addCallbackParam("loggedIn", loggedIn);
	            context.getExternalContext().getFlash().setKeepMessages(true);
	            context.getExternalContext().redirect("principalAdmin.xhtml");
			}else {
				context.addMessage(null, new FacesMessage("Ocurrio un error, intentelo mas tarde"));
		        PrimeFaces.current().ajax().addCallbackParam("loggedIn", loggedIn);
			}
			
			

	}
	public void NotiArtista(String nick) {
		this.setArtiNoti(nick);
	}
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void nuevaNoti() {
        boolean loggedIn = true;
		FacesContext context = FacesContext.getCurrentInstance();  

		
		System.err.println("A veerrrrrr-->"+ artiNoti);
			Artista a = usuarioDAO.buscarArtista(artiNoti);
			if(a!=null) {
				
				a.getNotificaciones().add("Admin: "+notiMensaj);
				int i = a.getNotificacionesNuevas() +1;
				a.setNotificacionesNuevas(i);
				em.getTransaction().begin();
				em.merge(a);
				em.getTransaction().commit();
				em.clear();
		        context.addMessage(null, new FacesMessage("Artista Notificado"));
		        PrimeFaces.current().ajax().addCallbackParam("loggedIn", loggedIn);
			}else {
		        context.addMessage(null, new FacesMessage("Se presento un error, intentelo mas tarde"));
		        PrimeFaces.current().ajax().addCallbackParam("loggedIn", loggedIn);
			}

	}
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void banArtista(String nick)throws IOException  {
        boolean loggedIn = true;
		FacesContext context = FacesContext.getCurrentInstance();  
		if(!nick.equals("")) {
			System.err.println("Porque-->"+nick);
			Artista a = usuarioDAO.buscarArtista(nick);
			if(a!=null) {
				a.setBan(true);
				em.getTransaction().begin();
				em.merge(a);
				em.getTransaction().commit();
				em.clear();
		        context.addMessage(null, new FacesMessage("Artista Baneado"));
		        PrimeFaces.current().ajax().addCallbackParam("loggedIn", loggedIn);
	            context.getExternalContext().getFlash().setKeepMessages(true);
	            context.getExternalContext().redirect("principalAdmin.xhtml");
			}else {
		        context.addMessage(null, new FacesMessage("Ocurrio un error, intentalo mas tarde"));
		        PrimeFaces.current().ajax().addCallbackParam("loggedIn", loggedIn);
			}

		}


	}
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void unbanArtista(String nick)throws IOException  {
        boolean loggedIn = true;
		FacesContext context = FacesContext.getCurrentInstance();  
		if(!nick.equals("")) {
		Artista a2 = usuarioDAO.buscarArtistaBan(nick);
		System.err.println("Aqui llega");
		if(a2!=null) {
			a2.setBan(false);
			em.getTransaction().begin();
			em.merge(a2);
			em.getTransaction().commit();
			em.clear();
	        context.addMessage(null, new FacesMessage("Artista Desbaneado"));
	        PrimeFaces.current().ajax().addCallbackParam("loggedIn", loggedIn);
            context.getExternalContext().getFlash().setKeepMessages(true);
            context.getExternalContext().redirect("principalAdmin.xhtml");
		}else {
	        context.addMessage(null, new FacesMessage("Ocurrio un error, intentalo mas tarde"));
	        PrimeFaces.current().ajax().addCallbackParam("loggedIn", loggedIn);
		}

		}
	}
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void nuevaCat() throws IOException {
        boolean loggedIn = true;
		FacesContext context = FacesContext.getCurrentInstance();  	
	
		boolean b= false;
		List<String> cates = usuarioDAO.listarCategorias();
		for(int i=0;i<=cates.size()-1;i++) {
			System.out.println(cates.get(i));
			if(cates.get(i).equals(cate)) {
				b = true;
			}
		}
		if(b == true) {
			loggedIn = false;
	        context.addMessage(null, new FacesMessage("Esa Categoria ya Existe"));
	        PrimeFaces.current().ajax().addCallbackParam("loggedIn", loggedIn);
		}else {
			Categoria c = new Categoria(cate,false);
						
			em.getTransaction().begin();
			em.persist(c);
			em.getTransaction().commit();
			em.clear();
	        context.addMessage(null, new FacesMessage("Categoria Creada Exitosamente"));
	        PrimeFaces.current().ajax().addCallbackParam("loggedIn", loggedIn);
            context.getExternalContext().getFlash().setKeepMessages(true);
            context.getExternalContext().redirect("principalAdmin.xhtml");
		}

		

	}
	public String getFechaCaducidad() {
		return FechaCaducidad;
	}
	public void setFechaCaducidad(String fechaCaducidad) {
		FechaCaducidad = fechaCaducidad;
	}
	
	public String cargFech(String idA) {

       Artista a = em.find(Artista.class,idA);
       
       List<mensualidad> fa22 = (List<mensualidad>) em.createNativeQuery("SELECT * FROM mensualidad", mensualidad.class).getResultList();

		for (mensualidad aux2 : fa22) {
			if (aux2.getNick().equals(a.getNick())) {
				Date d = aux2.getFechaCaducidad();
				DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
				String strDate = dateFormat.format(d);
				return strDate;

			}
       
       
		}
		return "no se a hecho el pago mensual";
	}
	
}
