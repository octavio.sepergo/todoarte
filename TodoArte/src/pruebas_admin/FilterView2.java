package pruebas_admin;

import java.io.Serializable;
import java.util.List;
import java.util.Locale;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.faces.view.ViewScoped;

import javax.inject.Named;

import clases.Artista;
import clases.Categoria;
import interfaces.UsuarioDAO;

@Named("dtFilterView2")
@RequestScoped
public class FilterView2 implements Serializable {
 
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private List<Categoria> cat;
    private List<Artista> filteredArt3;
    @EJB
    private UsuarioDAO service;
 
    @PostConstruct
    public void init() {
        cat = service.listarCategoriasAdmin();
    }
 
    public boolean globalFilterFunction3(Object value, Object filter3, Locale locale) {
        String filterText3 = (filter3 == null) ? null : filter3.toString().trim().toLowerCase();
        if (filterText3 == null || filterText3.equals("")) {
            return true;
        }
 
        Categoria ar = (Categoria) value;
        return ar.getNombre().toLowerCase().contains(filterText3);
    }
 
	
	public List<Categoria> getCat() {
		return cat;
	}

	public void setCat(List<Categoria> cat) {
		this.cat = cat;
	}


	public List<Artista> getFilteredArt3() {
		return filteredArt3;
	}

	public void setFilteredArt3(List<Artista> filteredArt3) {
		this.filteredArt3 = filteredArt3;
	}
    
	

}