package pruebas_admin;

import java.io.Serializable;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;

import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import clases.Artista;
import clases.Categoria;
import clases.Contenido;
import interfaces.SitioDAO;
import interfaces.UsuarioDAO;

@Named(value= "dtFilterView")
@ViewScoped
public class FilterView implements Serializable {
 
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private EntityManagerFactory emf = Persistence.createEntityManagerFactory("TodoArte");
	private EntityManager em = emf.createEntityManager();
	private List<Artista> art;
	private List<Artista> art2;
	private List<Categoria> cat;
    private List<Artista> filteredArt1;
    private List<Artista> filteredArt2;
    private List<Categoria> filteredArt3;
    private List<Contenido> conts;
    private List<Contenido> contsban;
    private List<String> reportes;
    @EJB
    private UsuarioDAO service;
    @EJB
    private SitioDAO sitio;
    @PostConstruct
    public void init() {
        art = service.listarArtistasUnban();
        art2 = service.listarArtistasBan();
        cat = service.listarCategoriasAdmin();
        conts = sitio.listarContenidoReportUnBan();
        contsban = sitio.listarContenidoBan();
        
    }
    
    
    public List<Artista> artistasunban(){
    	return this.art = service.listarArtistasUnban();
    }
    public List<Artista> artistasban(){
    	return this.art2 = service.listarArtistasBan();
    }
    public List<Contenido> contenidoreport(){
    	return this.conts = sitio.listarContenidoReportUnBan();
    }
    public List<Contenido> contenidoreport2(){
    	return this.contsban = sitio.listarContenidoReportUnBan();
    }
    
    public void reporteslist(){
    	
    	FacesContext fc = FacesContext.getCurrentInstance();
        Map<String,String> params = 
           fc.getExternalContext().getRequestParameterMap();
        int idr = Integer.valueOf(params.get("idR")) ;
        
    	Contenido content2 = em.find(Contenido.class, idr);
		System.out.println(content2.getReportes());

        System.out.println("aaaaaaaaaaaaaaaaa");
        System.out.println(idr);
        System.out.println("aaaaaaaaaaaaaaaaa");
		Contenido content = em.find(Contenido.class, idr);
		System.out.println(content.getReportes().isEmpty());
		System.out.println(content.getReportes());
    	this.reportes = content.getReportes();
    	System.out.println(this.reportes);
    }
    
    public boolean globalFilterFunction(Object value, Object filter, Locale locale) {
        String filterText = (filter == null) ? null : filter.toString().trim().toLowerCase();
        if (filterText == null || filterText.equals("")) {
            return true;
        }
 
        Artista ar = (Artista) value;
        return ar.getNick().toLowerCase().contains(filterText)
                || ar.getNombre().toLowerCase().contains(filterText)
                || ar.getCorreo().toLowerCase().contains(filterText);
    }
 
    public boolean globalFilterFunction2(Object value, Object filter, Locale locale) {
        String filterText = (filter == null) ? null : filter.toString().trim().toLowerCase();
        if (filterText == null || filterText.equals("")) {
            return true;
        }
 
        Artista ar2 = (Artista) value;
        return ar2.getNick().toLowerCase().contains(filterText)
                || ar2.getNombre().toLowerCase().contains(filterText)
                || ar2.getCorreo().toLowerCase().contains(filterText);
    }
 
    public boolean globalFilterFunction3(Object value, Object filter3, Locale locale) {
        String filterText3 = (filter3 == null) ? null : filter3.toString().trim().toLowerCase();
        if (filterText3 == null || filterText3.equals("")) {
            return true;
        }
 
        Categoria ar = (Categoria) value;
        return ar.getNombre().toLowerCase().contains(filterText3);
    }
 
	public List<Artista> getArt() {
		return art;
	}

	public void setArt(List<Artista> art) {
		this.art = art;
	}

	public List<Artista> getFilteredArt1() {
		return filteredArt1;
	}

	public void setFilteredArt1(List<Artista> filteredArt1) {
		this.filteredArt1 = filteredArt1;
	}

	public UsuarioDAO getService() {
		return service;
	}

	public void setService(UsuarioDAO service) {
		this.service = service;
	}

	public List<Categoria> getCat() {
		return cat;
	}

	public void setCat(List<Categoria> cat) {
		this.cat = cat;
	}

	public List<Artista> getArt2() {
		return art2;
	}

	public void setArt2(List<Artista> art2) {
		this.art2 = art2;
	}

	public List<Artista> getFilteredArt2() {
		return filteredArt2;
	}

	public void setFilteredArt2(List<Artista> filteredArt2) {
		this.filteredArt2 = filteredArt2;
	}

	public List<Categoria> getFilteredArt3() {
		return filteredArt3;
	}

	public void setFilteredArt3(List<Categoria> filteredArt3) {
		this.filteredArt3 = filteredArt3;
	}
	

	public EntityManagerFactory getEmf() {
		return emf;
	}


	public void setEmf(EntityManagerFactory emf) {
		this.emf = emf;
	}


	public EntityManager getEm() {
		return em;
	}


	public void setEm(EntityManager em) {
		this.em = em;
	}


	public List<Contenido> getConts() {
		return conts;
	}

	public void setConts(List<Contenido> conts) {
		this.conts = conts;
	}

	public SitioDAO getSitio() {
		return sitio;
	}

	public void setSitio(SitioDAO sitio) {
		this.sitio = sitio;
	}

	public List<Contenido> getContsban() {
		return contsban;
	}

	public void setContsban(List<Contenido> contsban) {
		this.contsban = contsban;
	}


	public List<String> getReportes() {
		return reportes;
	}


	public void setReportes(List<String> reportes) {
		this.reportes = reportes;
	}


    
	

}