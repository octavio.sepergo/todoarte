package pruebas_admin;

import javax.annotation.PostConstruct;
import javax.crypto.NoSuchPaddingException;
import javax.ejb.EJB;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.primefaces.PrimeFaces;

import clases.Admin;
import clases.Artista;
import clases.Fan;
import clases.Usuario;
import interfaces.ServiciosSeguridad;
import interfaces.UsuarioDAO;

import java.io.IOException;
import java.io.Serializable;
import java.security.NoSuchAlgorithmException;

@Named("LoginAdmin")
@SessionScoped
@TransactionManagement(TransactionManagementType.BEAN)
public class LoginAdmin  implements Serializable{


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String nick;
	private String pass;
	@EJB
	private ServiciosSeguridad servicioSeguridad;


	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public ServiciosSeguridad getServicioSeguridad() {
		return servicioSeguridad;
	}

	public void setServicioSeguridad(ServiciosSeguridad servicioSeguridad) {
		this.servicioSeguridad = servicioSeguridad;
	}

    public void login() throws IOException  {
    	FacesMessage message = null;
        boolean loggedIn = false;
        Admin ad = servicioSeguridad.existeAdmin(nick, pass);
        FacesContext context = FacesContext.getCurrentInstance();  
        if (ad == null) {
            context.addMessage(null, new FacesMessage("Datos incorrectos, intentelo de nuevo"));
            PrimeFaces.current().ajax().addCallbackParam("loggedIn", loggedIn);
            nick = null;
            pass = null;
           
        } else {
        	
                context.getExternalContext().getSessionMap().put("admin", ad); 
                loggedIn = true;
                message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Bienvenido "+"   "+nick, nick);
                FacesContext.getCurrentInstance().addMessage(null, message);
                PrimeFaces.current().ajax().addCallbackParam("loggedIn", loggedIn);
                context.getExternalContext().getFlash().setKeepMessages(true);
                context.getExternalContext().redirect("principalAdmin.xhtml");
        }
     
    }

    public String logout() {
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        return "loginAdmin.xhtml?faces-redirect=true";
    }

}
