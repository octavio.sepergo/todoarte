package service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.push.Push;
import javax.faces.push.PushContext;
import javax.inject.Inject;

import clases.Usuario;



@ApplicationScoped
public class ChatRoom {
	private static final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");


	private Logger LOG;

	// creates a Endpoint on the fly

	@Push(channel = "chatChannel")
	private PushContext chatChannel;


	@Push(channel = "clock")
	private PushContext clockChannel;

	private String chatHistory = "";

	public ChatRoom() {
	}

	@PostConstruct
	public void postConstruct() {
		if (chatChannel != null)
			LOG.info("Channel: " + chatChannel);
		else
			LOG.info("Channel is null");
	}

	public void notifyNewUser(Usuario user) {
		LOG.info("New user in the room: " + user.getNick());
	}
	
	public String getChatHistory() {
		return chatHistory;
	}

	public void broadcastMessage(String user, String message) {
		String fullMessage = String.format("%s: %s", user, message);
		LOG.info("Broadcasting: " + fullMessage);
		chatChannel.send(fullMessage);
		chatHistory = chatHistory + "\n" + fullMessage;
	}

	public void broadcastClock() {
		clockChannel.send(LocalDateTime.now().format(DATE_FORMAT));
	}
}
